﻿
using System.Configuration;
using CWC.Lib.Repositories.Contarcts;
using MongoDB.Driver;

namespace CWC.Lib.Repositories.Implementations
{

    public class MongoRepository<T> : IRepository<T> where T : class
    {

        protected readonly MongoCollection<T> Collection;

        public MongoRepository()
        {
            var con = new MongoConnectionStringBuilder(ConfigurationManager.ConnectionStrings["MongoDB"].ConnectionString);
            //var con = new MongoConnectionStringBuilder("server=127.0.0.1:27017;database=bitbook");
            #pragma warning disable 612,618
            var server = MongoServer.Create(con);
            #pragma warning restore 612,618
            var db = server.GetDatabase(con.DatabaseName);
            Collection = db.GetCollection<T>(typeof(T).Name.ToLower());
        }

        public MongoCollection<T> GetCollection()
        {
            return Collection;
        }

    }
    
}
