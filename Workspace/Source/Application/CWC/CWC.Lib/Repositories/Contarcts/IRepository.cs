﻿

using MongoDB.Driver;

namespace CWC.Lib.Repositories.Contarcts
{
    public interface IRepository<T> where T : class
    {
        MongoCollection<T> GetCollection();
    }
}
