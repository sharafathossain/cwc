﻿
namespace CWC.Data.Entity
{
    public class Login
    {

        public string LoginId { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string RoleId { get; set; }
        public virtual Role Role { get; set; }

    }
}
