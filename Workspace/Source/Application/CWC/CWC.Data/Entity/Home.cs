﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace CWC.Data.Entity
{
    public class Home
    {

        public Home()
        {
            Comments = new List<Comment>();
        }

        [ScaffoldColumn(false)]
        [BsonId]
        public ObjectId HomeId { get; set; }

        [Required]
        public string HomeStatus { get; set; }

        [ScaffoldColumn(false)]
        public IList<Comment> Comments { get; set; }

    }
}
