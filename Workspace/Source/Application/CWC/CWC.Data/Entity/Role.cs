﻿
using System.Collections.Generic;

namespace CWC.Data.Entity
{
    public class Role
    {

        public Role()
        {
            this.Logins = new HashSet<Login>();
        }

        public string RoleId { get; set; }
        public string RoleDescription { get; set; }

        public virtual ICollection<Login> Logins { get; set; }

    }
}
