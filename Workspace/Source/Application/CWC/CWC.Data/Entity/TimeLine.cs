﻿

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace CWC.Data.Entity
{
    public class TimeLine
    {

        public TimeLine()
        {
            LikeUsers = new List<string>();
        }

        [ScaffoldColumn(false)]
        [BsonId]
        public ObjectId TimeLineId { get; set; }

        [Required]
        public string Id { get; set; }

        [Required]
        public string Status { get; set; }

        [Required]
        public long Like { get; set; }

        [Required]
        public string UserId { get; set; }

        [Required]
        public string ContentPath { get; set; }

        [Required]
        public DateTime CreatedOn { get; set; }

        public IList<string> LikeUsers { get; set; }

       
        
    }
}
