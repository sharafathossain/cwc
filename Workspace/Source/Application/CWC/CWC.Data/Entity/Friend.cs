﻿

using System.ComponentModel.DataAnnotations;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace CWC.Data.Entity
{
    public class Friend
    {

        [ScaffoldColumn(false)]
        [BsonId]
        public ObjectId FId { get; set; }

        [Required]
        public string FriendId { get; set; }

        [Required]
        public string Status { get; set; }

        [Required]
        public string UserId { get; set; }


       
        
    }
}
