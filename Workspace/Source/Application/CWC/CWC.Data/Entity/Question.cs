﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace CWC.Data.Entity
{
    public class Question
    {

        public Question()
        {
            Comments = new List<Comment>();
        }

        [ScaffoldColumn(false)]
        [BsonId]
        public ObjectId QuestionId { get; set; }

        [Required]
        public string QuestionDescription { get; set; }

        [ScaffoldColumn(false)]
        public IList<Comment> Comments { get; set; }

    }
}
