﻿
using CWC.Biz.Contracts.App;
using CWC.Biz.Implementations.App;
using CWC.Biz.Models;
using CWC.Biz.Models.App;
using CWC.Data.Entity;
using CWC.Lib.Repositories.Contarcts;
using CWC.Lib.Repositories.Implementations;
using NUnit.Framework;

namespace CWC.Biz.Test.QuestionTest
{
    [TestFixture]
    class AccountManagerTest
    {
        private IRepository<User> _userRepository = null;
        private IRepository<Friend> _friendRepository = null;
        private IUserManager _accountManager = null;

        [SetUp]
        public void Init()
        {
            _userRepository = new MongoRepository<User>();
            _friendRepository = new MongoRepository<Friend>();
            _accountManager = new UserManager(_userRepository, _friendRepository);
        }

        [TearDown]
        public void Destroy()
        {
            
        }

        [Test]
        public void CreateAccount()
        {
            var res = new ResViewModel();

            var user = new UserViewModel();
            user.FirstName = "Tanim";
            _accountManager.CreateUserAccount(res, user);
        }

        [Test]
        public void ConvertTest()
        {
           
        }


    }
}
