﻿

using System.Collections.Generic;
using CWC.Data.Entity;
using NUnit.Framework;
using CWC.Lib.Repositories.Contarcts;
using CWC.Lib.Repositories.Implementations;

namespace CWC.Biz.Test.DummyDataCreateTest
{
    [TestFixture]
    public class UserDummyData
    {
        private IRepository<User> _userRepository;

        [SetUp]
        public void Init()
        {
            _userRepository = new MongoRepository<User>();
        }

        [Test]
        public void CreateUserDummyData()
        {

            /*for (int i = 1; i <= 3; i++ )
            {
                var user = new User();
                user.FirstName = "FirstName" + i;
                user.LastName = "LastName"+i;
                user.Email = user.FirstName.ToLower() + "@spectrum-bd.com";
                user.Password = "user"+i;
                user.Gender = "Male";
                user.Id = i.ToString();
                user.PhotoPath = "../../Images/Imgs/maleuser.jpg";

                _userRepository.GetCollection().Save(user);
            }
            Assert.AreEqual(3, _userRepository.GetCollection().Count()); */
            foreach (var user in GetUsers() )
            {
                _userRepository.GetCollection().Save(user);
            }
            Assert.AreEqual(4, _userRepository.GetCollection().Count());
        }

        [TearDown]
        public void Destroy()
        {
            _userRepository = null;
        }


        private IEnumerable<User> GetUsers()
        {
            IList<User> users = new List<User>();
            var user1 = new User();
            user1.FirstName = "Jamal";
            user1.LastName = "Uddin";
            user1.Email = "jamal@spectrum-bd.com";
            user1.Password = "123";
            user1.Gender = "Male";
            user1.Id = "jamal";
            user1.PhotoPath = "../../Images/Imgs/jamal@spectrum-bd.com.png";
            users.Add(user1);

            var user2 = new User();
            user2.FirstName = "Kamruzzaman";
            user2.LastName = "Tanim";
            user2.Email = "ktanim@spectrum-bd.com";
            user2.Password = "123";
            user2.Gender = "Male";
            user2.Id = "ktanim";
            user2.PhotoPath = "../../Images/Imgs/ktanim@spectrum-bd.com.png";
            users.Add(user2);

            var user3 = new User();
            user3.FirstName = "Sharafat";
            user3.LastName = "hossain";
            user3.Email = "sharafathossain@spectrum-bd.com";
            user3.Password = "123";
            user3.Gender = "Male";
            user3.Id = "sharafat";
            user3.PhotoPath = "../../Images/Imgs/sharafathossain@spectrum-bd.com.png";
            users.Add(user3);

            var user4 = new User();
            user4.FirstName = "Kowsar";
            user4.LastName = "Bhuyian";
            user4.Email = "kowsar@spectrum-bd.com";
            user4.Password = "123";
            user4.Gender = "Male";
            user4.Id = "kowsar";
            user4.PhotoPath = "../../Images/Imgs/kowsar@spectrum-bd.com.png";
            users.Add(user4);

            return users;
        }

    }
}
