﻿
using System.Collections.Generic;
using System.Web.Http;
using CWC.Biz.Contracts.Security;
using CWC.Biz.Models;
using CWC.Biz.Models.Security;

namespace CWC.Web.Controllers.Security
{
    public class SignInController : ApiController
    {
        private readonly ISignInManager _signInManager;

        public SignInController(ISignInManager signInManager)
        {
            _signInManager = signInManager;
        }


        // GET api/signin
        public IEnumerable<string> Get()
        {
            return new[] { "value1", "value2" };
        }

        // GET api/signin/5
        public string Get(int id)
        {
            return "value";
        }

        // PUT api/signin/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // POST api/signin
        public ResViewModel Post(SignInViewModel model)
        {
            if (model.OperationType.Equals("SignIn"))
            {
                return _signInManager.SignIn(model);
            }
            if (model.OperationType.Equals("SignOut"))
            {
                return _signInManager.SignOut(model);
            }
            return null;
        }

        // DELETE api/signin/5
        public void Delete(int id)
        {
        }

    }
}
