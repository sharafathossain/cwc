﻿
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using CWC.Biz.Contracts.App;
using CWC.Biz.Models;
using CWC.Biz.Models.App;
using CWC.Biz.Utils;
using CWC.Web.Utils;

namespace CWC.Web.Controllers.App
{
    public class PendingUserController : ApiController
    {
        private readonly IUserManager _accountManager;

        public PendingUserController(IUserManager accountManager)
        {
            _accountManager = accountManager;
        }


        // GET api/user
        public DataTableResModel Get(HttpRequestMessage req)
        {
            var paramModel = DataTablesUtil.GetDataTableParamModel(req);
            return _accountManager.GetPendingUsers(paramModel);
        }

        // PUT api/user/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // POST api/user
        public ResViewModel Post(UserViewModel model)
        {
            var resViewModel = new ResViewModel();

            if (model.OperationType.Equals("CreateAccount"))
            {
                return _accountManager.CreateUserAccount(resViewModel, model);
            }
            if (model.OperationType.Equals("ChangePassword"))
            {
                return _accountManager.ChangePassword(resViewModel, model);
            } 
            if (model.OperationType.Equals("FriendRequest"))
            {
                return _accountManager.AddFriend(resViewModel, model);
            }
            return resViewModel;
        }

        // DELETE api/user/5
        public void Delete(int id)
        {
        }

    }
}
