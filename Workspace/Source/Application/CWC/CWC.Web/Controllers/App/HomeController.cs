﻿
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using CWC.Biz.Contracts.App;
using CWC.Biz.Models;
using CWC.Biz.Models.App;
using CWC.Biz.Utils;
using CWC.Web.Utils;

namespace CWC.Web.Controllers.App
{
    public class HomeController : ApiController
    {
        private readonly ITimeLineManager _homeManager;

        public HomeController(ITimeLineManager homeManager)
        {
            _homeManager = homeManager;
        }

        // GET api/home
        public DataTableResModel Get(HttpRequestMessage req)
        {
            var paramModel = DataTablesUtil.GetDataTableParamModel(req);
            return _homeManager.GetTimeLines(paramModel);
        }

        // PUT api/home/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // POST api/poststatus
        public ResViewModel Post(TimeLineViewModel model)
        {
            var resViewModel = new ResViewModel();

            if (model.OperationType.Equals("SaveStatus"))
            {
                return _homeManager.SaveTimeLine(resViewModel, model);
            }
            if (model.OperationType.Equals("LikeCount"))
            {
                return _homeManager.LikeCount(resViewModel, model);
            }
            if (model.OperationType.Equals("Delete"))
            {
                return _homeManager.DeletePost(resViewModel, model);
            }
            return resViewModel;
        }

        // DELETE api/poststatus/5
        public void Delete(int id)
        {
        }

    }
}
