﻿
using System.Collections.Generic;
using System.Web.Http;
using CWC.Biz.Contracts.App;
using CWC.Biz.Models;
using CWC.Biz.Models.App;

namespace CWC.Web.Controllers.App
{
    public class QuestionController : ApiController
    {
        private readonly IQuestionManager _questionManager;

        public QuestionController(IQuestionManager questionManager)
        {
            _questionManager = questionManager;
        }

        // GET api/question
        public IEnumerable<string> Get()
        {
            return new[] { "q1", "q2" };
        }

        // GET api/question/5
        public string Get(int id)
        {
            return "question5";
        }

        // PUT api/question/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // POST api/question
        public ResViewModel Post(QuestionViewModel model)
        {
            var resViewModel = new ResViewModel();

            if (model.OperationType.Equals("SaveQuestion"))
            {
                return _questionManager.SaveQuestion(resViewModel, model);
            }
            return resViewModel;
        }

        // DELETE api/question/5
        public void Delete(int id)
        {
        }

    }
}
