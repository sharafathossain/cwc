﻿using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;
using CWC.Biz.Contracts.App;
using CWC.Biz.Implementations.App;
using CWC.Biz.Models.App;
using CWC.Data.Entity;
using CWC.Lib.Repositories.Contarcts;
using CWC.Lib.Repositories.Implementations;
using Microsoft.AspNet.SignalR;

namespace CWC.Web
{
    public class FriendHub : Hub
    {
        private static IRepository<Question>  _questionRepository = new MongoRepository<Question>();
        private static IQuestionManager _db = new QuestionManager(_questionRepository);
        private static ConcurrentDictionary<string, int> _locks = new ConcurrentDictionary<string, int>();
        private static object _lock = new object();

        public override async Task OnConnected()
        {
            var query = _questionRepository.GetCollection();

            await Clients.Caller.all(query);
            await Clients.Caller.allLocks(_locks.Values);
        }

        public override async Task OnReconnected()
        {
            // Refresh as other users could update data while we were offline
            await OnConnected();
        }

        public override async Task OnDisconnected()
        {
            int removed;
            if(_locks.TryRemove(Context.ConnectionId, out removed))
            {
                await Clients.All.allLocks(_locks.Values);
            }
        }



        public void Add(QuestionViewModel value)
        {
            value.QuestionId = "1234";
            Clients.All.add(value);
        }

        
    }
}