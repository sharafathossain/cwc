﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using CWC.Biz.Models.Security;
using CWC.Biz.Utils;
using Newtonsoft.Json;

namespace CWC.Web.Utils
{
    public class DataTablesUtil {

        public static DataTableResModel GetResDataTable(DataTableParamModel paramModel, IEnumerable<object> modelList)
        {
            var resDataTable = new DataTableResModel();
            if (modelList == null)
            {
                return resDataTable;
            }
            resDataTable.sEcho = paramModel.sEcho;
            resDataTable.iTotalRecords = modelList.Count();
            resDataTable.iTotalDisplayRecords = modelList.Count();
            resDataTable.aaData = modelList;
            return resDataTable;
        }

        public static DataTableParamModel GetDataTableParamModel(HttpRequestMessage req)
        {
            var model = new DataTableParamModel();

            var queryStrings = req.GetQueryNameValuePairs();
            if (queryStrings == null)
                return model;
            var sEchoValue = queryStrings.FirstOrDefault(kv => System.String.Compare(kv.Key, "sEcho", System.StringComparison.OrdinalIgnoreCase) == 0);
            if (!string.IsNullOrEmpty(sEchoValue.Value))
                model.sEcho = Convert.ToInt32(sEchoValue.Value);

            var iColumnsValue = queryStrings.FirstOrDefault(kv => System.String.Compare(kv.Key, "iColumns", System.StringComparison.OrdinalIgnoreCase) == 0);
            if (!string.IsNullOrEmpty(iColumnsValue.Value))
                model.iColumns = Convert.ToInt32(iColumnsValue.Value);

            var sColumnsValue = queryStrings.FirstOrDefault(kv => System.String.Compare(kv.Key, "sColumns", System.StringComparison.OrdinalIgnoreCase) == 0);
            if (!string.IsNullOrEmpty(sColumnsValue.Value))
                model.sColumns = Convert.ToInt32(sColumnsValue.Value);

            var iDisplayStartValue = queryStrings.FirstOrDefault(kv => System.String.Compare(kv.Key, "iDisplayStart", System.StringComparison.OrdinalIgnoreCase) == 0);
            if (!string.IsNullOrEmpty(iDisplayStartValue.Value))
                model.iDisplayStart = Convert.ToInt32(iDisplayStartValue.Value);

            var iDisplayLengthValue = queryStrings.FirstOrDefault(kv => System.String.Compare(kv.Key, "iDisplayLength", System.StringComparison.OrdinalIgnoreCase) == 0);
            if (!string.IsNullOrEmpty(iDisplayLengthValue.Value))
                model.iDisplayLength = Convert.ToInt32(iDisplayLengthValue.Value);

            var iSortCol0Value = queryStrings.FirstOrDefault(kv => System.String.Compare(kv.Key, "iSortCol_0", System.StringComparison.OrdinalIgnoreCase) == 0);
            if (!string.IsNullOrEmpty(iSortCol0Value.Value))
                model.iSortCol_0 = Convert.ToInt32(iSortCol0Value.Value);

            var sSortDir0Value = queryStrings.FirstOrDefault(kv => System.String.Compare(kv.Key, "sSortDir_0", System.StringComparison.OrdinalIgnoreCase) == 0);
            model.sSortDir_0 = sSortDir0Value.Value;

            var sSearchValue = queryStrings.FirstOrDefault(kv => System.String.Compare(kv.Key, "sSearch", System.StringComparison.OrdinalIgnoreCase) == 0);
            model.sSearch = sSearchValue.Value;

            var bRegexValue = queryStrings.FirstOrDefault(kv => System.String.Compare(kv.Key, "bRegex", System.StringComparison.OrdinalIgnoreCase) == 0);
            if (!string.IsNullOrEmpty(bRegexValue.Value))
                model.bRegex = Convert.ToBoolean(bRegexValue.Value);

            var iSortingColsValue = queryStrings.FirstOrDefault(kv => System.String.Compare(kv.Key, "iSortingCols", System.StringComparison.OrdinalIgnoreCase) == 0);
            if (!string.IsNullOrEmpty(iSortingColsValue.Value))
                model.iSortingCols = Convert.ToInt32(iSortingColsValue.Value);

            var iSortColumnIndexValue = queryStrings.FirstOrDefault(kv => System.String.Compare(kv.Key, "iSortColumnIndex", System.StringComparison.OrdinalIgnoreCase) == 0);
            if (!string.IsNullOrEmpty(iSortColumnIndexValue.Value))
                model.iSortColumnIndex = Convert.ToInt32(iSortColumnIndexValue.Value);

            var sSortDirectionValue = queryStrings.FirstOrDefault(kv => System.String.Compare(kv.Key, "sSortDirection", System.StringComparison.OrdinalIgnoreCase) == 0);
            model.sSortDirection = sSortDirectionValue.Value;

            var loginViewModel = queryStrings.FirstOrDefault(kv => System.String.Compare(kv.Key, "LoginViewModel", System.StringComparison.OrdinalIgnoreCase) == 0);
            if (!string.IsNullOrEmpty(loginViewModel.Value))
            {
                model.LoginViewModel = JsonConvert.DeserializeObject<LoginViewModel>(loginViewModel.Value);
            }
            

            return model;
        }


    }
}
