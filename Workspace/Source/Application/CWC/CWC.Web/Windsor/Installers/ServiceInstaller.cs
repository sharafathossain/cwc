﻿
using CWC.Biz.Contracts.App;
using CWC.Biz.Implementations.App;
using Castle.Core;
using Castle.Facilities.Logging;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using CWC.Biz.Contracts.Security;
using CWC.Biz.Implementations.Security;

namespace CWC.Web.Windsor.Installers
{
    public class ServiceInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.AddFacility<LoggingFacility>(f => f.UseLog4Net());
            container.AddFacility<MongoFacility>();

            container.Register(
                Component.For<ISignInManager>().ImplementedBy<SignInManager>().LifeStyle.Is(LifestyleType.PerWebRequest),
                Component.For<IQuestionManager>().ImplementedBy<QuestionManager>().LifeStyle.Is(LifestyleType.PerWebRequest),
                Component.For<IUserManager>().ImplementedBy<UserManager>().LifeStyle.Is(LifestyleType.PerWebRequest),
                Component.For<ITimeLineManager>().ImplementedBy<TimeLineManager>().LifeStyle.Is(LifestyleType.PerWebRequest)
            );
        }
    }
}