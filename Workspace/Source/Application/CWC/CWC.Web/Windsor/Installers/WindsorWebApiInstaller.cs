﻿
using System.Web.Http;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace CWC.Web.Windsor.Installers
{
    public class WindsorWebApiInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            if (container != null)
            #pragma warning disable 612,618
                container.Register(AllTypes.FromThisAssembly().BasedOn<ApiController>().LifestyleScoped());
            #pragma warning restore 612,618
        }
    }
}