﻿
using CWC.Lib.Repositories.Implementations;
using Castle.MicroKernel.Facilities;
using Castle.MicroKernel.Registration;
using CWC.Lib.Repositories.Contarcts;

namespace CWC.Web.Windsor.Installers
{
    public class MongoFacility : AbstractFacility
    {
        protected override void Init()
        {
            Kernel.Register(
                Component.For(typeof(IRepository<>)).ImplementedBy(typeof(MongoRepository<>)).LifestylePerWebRequest()
                );
        }
    }
}