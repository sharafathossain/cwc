﻿
'use strict';

cwcApp.service('navigationService', ['$location', '$window', function ($location, $window) {
    
    this.toHomePage = function () {
        $location.path('/');
        $location.replace();
    };

    this.showPage = function (url) {
        $location.path('/' + url);
    };
   
    
    
}]);
