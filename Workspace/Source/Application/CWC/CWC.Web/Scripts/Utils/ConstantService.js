﻿
'use strict';

cwcApp.service('constantService', ['$rootScope', function ($rootScope) {

    this.DefaultCode = 'Df1000';
    this.ErrorMessage = 'ErrorMessage';
    this.userInfoCookieStoreKey = 'user.info.cookie.store.key';
    
    this.getOverrideOptions = function (dataSource, userInfo) {
        var overrideOptions = {
            "sDom": '<"top"<"#buttonDiv"><"toolbar">f<"clear">>rt<"bottom"ip>',
            "aLengthMenu": [[5, 10, 15], [5, 10, 15]],
            "aaSorting": [[1, "asc"]],
            "iDisplayLength": 5,
            "bProcessing": false,
            "bServerSide": true,
            "sAjaxSource": dataSource,
            "sPaginationType": "full_numbers",
            "bStateSave": false,
            "bJQueryUI": false,
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bInfo": false,
            "bDestroy": true,
            "bRetrieve": true,
            "oLanguage": {
                "sProcessing": "Processing...",
                "sLengthMenu": "Rows: _MENU_ ",
                "sZeroRecords": "No matching records found",
                "sInfo": "Viewing _START_ - _END_ of _TOTAL_ ",
                "sInfoEmpty": "Viewing 0 - 0 of 0 ",
                "sInfoFiltered": "(filtered from _MAX_ total entries)",
                "sInfoPostFix": "",
                "sEmptyTable": "empty",
                "sLoadingRecords": "Loading...",
                "sSearch": "Find Friends",
                "oPaginate": {
                    "sFirst": "First",
                    "sPrevious": "Previous",
                    "sNext": "Next",
                    "sLast": "Last"
                },
                "oAria": {
                    "sSortAscending": " - click/return to sort ascending",
                    "sSortDescending": " - click/return to sort descending"
                }
            },
            "fnServerParams": function (aoData) {
                aoData.push({ "name": "LoginViewModel", "value": JSON.stringify(userInfo) });
            }
        };
        return overrideOptions;
    };
    
    this.getTimeLineOverrideOptions = function (dataSource, userInfo) {
        var overrideOptions = {
            "sDom": '<"top"<"#buttonDiv"><"toolbar"><"clear">>rt<"bottom">',
            "aLengthMenu": [[5, 10, 15], [5, 10, 15]],
            "aaSorting": [[1, "asc"]],
            "iDisplayLength": 5,
            "bProcessing": false,
            "bServerSide": true,
            "sAjaxSource": dataSource,
            "sPaginationType": "full_numbers",
            "bStateSave": false,
            "bJQueryUI": false,
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bInfo": false,
            "bDestroy": true,
            "bRetrieve": true,
            "oLanguage": {
                "sProcessing": "Processing...",
                "sLengthMenu": "Rows: _MENU_ ",
                "sZeroRecords": "No records found",
                "sInfo": "Viewing _START_ - _END_ of _TOTAL_ ",
                "sInfoEmpty": "Viewing 0 - 0 of 0 ",
                "sInfoFiltered": "(filtered from _MAX_ total entries)",
                "sInfoPostFix": "",
                "sEmptyTable": "empty",
                "sLoadingRecords": "Loading...",
                "sSearch": "",
                "oPaginate": {
                    "sFirst": "First",
                    "sPrevious": "Previous",
                    "sNext": "Next",
                    "sLast": "Last"
                },
                "oAria": {
                    "sSortAscending": " - click/return to sort ascending",
                    "sSortDescending": " - click/return to sort descending"
                }
            },
            "fnServerParams": function (aoData) {
                aoData.push({ "name": "LoginViewModel", "value": JSON.stringify(userInfo) });
            }
        };
        return overrideOptions;
    };

    this.setCustomToolbar = function () {
        $("#buttonDiv").empty();
        $("#buttonDiv").addClass("buttonDiv");
        $("#buttonDiv").append("<input id='btnNew' type='button' class='btn btn-info btn-primary' value='New'/>&nbsp;&nbsp;&nbsp;" +
            "<input Id='btnDelete' type='button' class='btn btn-danger btn-primary' value='Delete'/>");
        $("#buttonDiv").parent().find('div').eq(1).find('select').addClass("cmbDataTableLength");
    };
    
    this.selectedRowFromGrid = function () {
        var selectedRowArray = [];
        $('table.display tbody tr').each(function () {
            var selectionclassName = $(this).find("td").eq(0).find("div span a").attr("class");
            if (selectionclassName == "jqTransformCheckbox jqTransformChecked") {
                var facultyDeanInfoOid = $(this).find("td").eq(0).find("div span input").val();
                selectedRowArray.push(facultyDeanInfoOid);
            }
        });
        return selectedRowArray;
    };

    this.clickCheckBoxOnGridRow = function (nRow) {
        $('td:eq(0)', nRow).live('click', function () {
            var id = nRow.cells[0].children[0].children[0].children[1].value;
            var className = $("#" + id + "_checkbox").find("span a").attr("class");
            if (className == "jqTransformCheckbox jqTransformChecked") {
                $("#" + id + "_checkbox").find("span a").removeClass("jqTransformChecked");
            }
            else {
                $("#" + id + "_checkbox").find("span a").addClass("jqTransformChecked");
            }
            var isSelectAll = true;
            $('table.display tbody tr').each(function () {
                var selectionclassName = $(this).find("td").eq(0).find("div span a").attr("class");
                if (selectionclassName == "jqTransformCheckbox") {
                    isSelectAll = false;
                }
            });
            if (!isSelectAll) {
                $("#selectall_checkbox").removeClass("jqTransformChecked");
            }
            else {
                $("#selectall_checkbox").addClass("jqTransformChecked");
            }
        });
    };

    this.removeDataFromArray = function (array, property, value) {
        var myNewData = [];
        $.each(array, function (index, result) {
            if (result[property] != value) {
                myNewData.push(result);
            }
        });
        return myNewData;
    };
    
    this.getRandomId = function () {
        var date = new Date();
        var id = date.getTime() + '-' + (Math.random().toString(36).substr(2, 9));
        return id;
    };

}]);

