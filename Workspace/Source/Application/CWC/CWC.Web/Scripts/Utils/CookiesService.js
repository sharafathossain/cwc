﻿

cwcApp.service('cookiesService', ['$cookieStore', function ($cookieStore) {
    
    this.setValue = function (key, value) {
        $cookieStore.put(key, value);
    };

    this.getValue = function (key) {
        return $cookieStore.get(key);
    };
    
}]);
