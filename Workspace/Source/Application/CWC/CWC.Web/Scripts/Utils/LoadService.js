﻿'use strict';

cwcApp.service('loadService', [function () {
    
    // Use to show Loader
    this.showDialog = function() {
        $("#overlay").show();
        $("#dialog").html('<div Id="circularG"><div Id="circularG_1" class="circularG"></div><div Id="circularG_2" class="circularG"></div><div Id="circularG_3" class="circularG"></div><div Id="circularG_4" class="circularG"></div><div Id="circularG_5" class="circularG"></div><div Id="circularG_6" class="circularG"></div><div Id="circularG_7" class="circularG"></div><div Id="circularG_8" class="circularG"></div></div>');
        $("#dialog").show();
    };

    // Use to hIde Loader
    this.hideDialog = function () {
        $("#overlay").hide();
        $("#dialog").hide();
        $("#dialog").empty();
    };
   
    
    
}]);
