﻿

cwcApp.service('localStorageService', [function () {

    this.setData = function (key, data) {
        if (typeof (Storage) !== "undefined") {
            window.localStorage.setItem(key, JSON.stringify(data));
        }
    };
    
    this.getData = function (key) {
        if (typeof (Storage) !== "undefined") {
            return $.parseJSON(window.localStorage.getItem(key));
        }
    };
    


}]);
