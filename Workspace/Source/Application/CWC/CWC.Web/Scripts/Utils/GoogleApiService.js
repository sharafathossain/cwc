'use strict';
cwcApp.service('googleService', ['$http', '$rootScope', '$q', function ($http, $rootScope, $q) {
        var clientId = '1066335131262-j0j40fauqr6sfflrmusq3tr215siu94h.apps.googleusercontent.com',
            //apiKey = 'IwcdfObAyValbt2outU_e1RZ',
            scopes = 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/userinfo.email',
            domain = 'http://localhost',
            deferred = $q.defer();

        this.login = function () {
            gapi.auth.authorize({
                client_id: clientId,
                scope: scopes,
                immediate: false,
                hd: domain
            }, this.handleAuthResult);

            return deferred.promise;
        }

        this.handleClientLoad = function () {
            gapi.client.setApiKey(apiKey);
            gapi.auth.init(function () { });
            window.setTimeout(checkAuth, 1);
        };

        this.checkAuth = function () {
            gapi.auth.authorize({
                client_id: clientId,
                scope: scopes,
                immediate: true,
                hd: domain
            }, this.handleAuthResult);
        };

        this.handleAuthResult = function (authResult) {
            if (authResult && !authResult.error) {
                var data = {};
                gapi.client.load('oauth2', 'v2', function () {
                    var request = gapi.client.oauth2.userinfo.get();
                    request.execute(function (resp) {
                        data.userdetails = resp;
                    });
                });
                deferred.resolve(data);
            } else {
                deferred.reject('error');
            }
        };

        this.handleAuthClick = function (event) {
            gapi.auth.authorize({
                client_id: clientId,
                scope: scopes,
                immediate: false,
                hd: domain
            }, this.handleAuthResult);
            return false;
        };

    }]);

