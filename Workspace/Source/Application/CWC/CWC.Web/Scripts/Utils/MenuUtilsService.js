
'use strict';

cwcApp.service('menuUtilsService', ['$rootScope', function ($rootScope) {
    
    var topMenuItems = 
        [
            { 
                text: 'Home',
                url: 'home'
            },
            {
                text: 'Find Friends',
                url: 'findfriends'
            },
            {
                text: 'Friend Requests',
                url: 'pendingrequests'

            },
            {
                text: 'My Friends',
                url: 'friends'
            }
        ];

        
    this.getTopMenuItems = function () {
        return topMenuItems;
    };

    this.getLeftMenuItems = function (key) {
        for(var i=0;i<topMenuItems.length;i++){
            if(topMenuItems[i].url === key){
                return topMenuItems[i].leftMenuItems;
            }
        }
    };
    
}]);
