﻿
'use strict';

cwcApp.service('configurationService', [function () {

    this.server = 'localhost';
    this.bomChar = '\\';
    this.port = ':2162';
    this.api = '/api/';
    this.baseUrl = 'http://' + this.server + this.bomChar + this.port + this.api;
    this.signInWebBaseApiUrl = this.baseUrl + 'signin/';
    
    this.questionWebBaseApiUrl = this.baseUrl + 'question/';
    this.userWebBaseApiUrl = this.baseUrl + 'user/';
    this.signInWebApiUrl = this.signInWebBaseApiUrl + ':Id';
    this.questionWebApiUrl = this.questionWebBaseApiUrl + ':Id';
    this.userWebApiUrl = this.userWebBaseApiUrl + ':id';

    this.homeWebBaseApiUrl = this.baseUrl + 'home/';
    this.homeWebApiUrl = this.homeWebBaseApiUrl + ':Id';
    


    this.userDataGridUrl = this.api + 'user/get';
    this.timeLineDataGridUrl = this.api + 'home/get';

    this.pendingUserDataGridUrl = this.api + 'pendinguser/get';
    this.myFriendsDataGridUrl = this.api + 'myfriends/get';


    
}]);

