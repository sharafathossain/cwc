﻿'use strict';


cwcApp.directive('test', function ($compile) {
    return {
        restrict: 'E',
        scope: {
            text: '='
        },
        template: '<p ng-click="add()">{{text}}</p>',
        controller: function ($scope, $element) {
            $scope.add = function() {
                var el = $compile("<test text='n'></test>")($scope);
                $element.parent().append(el);
            };
        }
    };
    
});
