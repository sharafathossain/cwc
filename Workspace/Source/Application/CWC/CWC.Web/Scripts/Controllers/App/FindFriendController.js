﻿
'use strict';

cwcApp.controller('findFriendController', ['$rootScope', '$scope', '$log', 'loadService',
    'questionService', 'messageService', 'cookiesService', 'constantService', 'configurationService',
    'userService',
    function ($rootScope, $scope, $log, loadService, questionService, messageService, cookiesService,
    constantService, configurationService, userService) {

        var promis, userInfo;
        var chat = $.connection.notificationHub;

       

        var clickOnRow = function (userInformation, nRow, oid) {
            $('#sendrequest_' + oid, nRow).live('click', function () {
                $scope.$apply(function () {
                    sendRequest(userInformation, oid, 'Pending');
                    $.connection.hub.start().done(function () {
                        
                        chat.server.send("ABCD", userInformation.FirstName + " sent friend request");
                       });

                });
            });
            
            $('#cancelrequest_' + oid, nRow).live('click', function () {
                $scope.$apply(function () {
                    sendRequest(userInformation, oid, 'Delete');
                });
            });
            
            $('#acceptrequest_' + oid, nRow).live('click', function () {
                $scope.$apply(function () {
                    sendRequest(userInformation, oid, 'Accept');
                });
            });
            
            $('.findFriendsImage', nRow).live('click', function () {
                $scope.$apply(function () {
                    /*var modalOptions = {
                        closeButtonText: 'No',
                        actionButtonText: 'Yes',
                        headerText: 'Save Purchase?',
                        bodyText: 'Are you sure you want to save purchase?'
                    };
                    
                    modalService.showModal({}, modalOptions).then(function (result) {
                        loadService.showDialog();
                        

                    });*/


                });
            });

        };

        var sendRequest = function (userInformation, oid, status) {
            userInformation.OperationType = 'FriendRequest';
            userInformation.Id = oid;
            userInformation.Status = status;
            //loadService.showDialog();
            promis = userService.addFriend(userInformation);
            promis.then(function (data) {
                if (!data.IsSuccess) {
                    loadService.hideDialog();
                    messageService.openErrorMessage(data.Code);
                    return;
                }
                messageService.openSuccessMessage(data.Code);
                $scope.reloadGrid();
                //loadService.hideDialog();

               

            });
        };

        

        $scope.myRowCallback = function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            var oid = nRow.cells[1].children[0].children[2].children[0].attributes['alt'].nodeValue;
            userInfo = cookiesService.getValue(constantService.userInfoCookieStoreKey);
            //constantService.clickCheckBoxOnGridRow(nRow);
            clickOnRow(userInfo, nRow, oid);
            return nRow;
        };

        $scope.myDrawCallback = function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            userInfo = cookiesService.getValue(configurationService.loginCookieStoreKey);
            //constantService.setCustomToolbar(userInfo);
            return nRow;
        };

        var load = function () {
            userInfo = cookiesService.getValue(constantService.userInfoCookieStoreKey);
            $scope.overrideOptions = constantService.getOverrideOptions(configurationService.userDataGridUrl, userInfo);
            $scope.columnDefs = userService.getColumnDefs(userInfo);
        };

        load();
        

        
}]);