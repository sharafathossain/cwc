﻿
'use strict';

cwcApp.controller('pendingRequestController', ['$rootScope', '$scope', '$log', 'loadService',
    'questionService', 'messageService', 'cookiesService', 'constantService', 'configurationService',
    'userService', 'pendingRequestService',
    function ($rootScope, $scope, $log, loadService, questionService, messageService, cookiesService,
    constantService, configurationService, userService, pendingRequestService) {

        var promis, userInfo;
        var chat = $.connection.notificationHub;
        
        var clickOnRow = function (userInformation, nRow, oid) {
            $('#sendrequest_' + oid, nRow).live('click', function () {
                $scope.$apply(function () {
                    sendRequest(userInformation, oid, 'Pending');
                });
            });
            
            $('#cancelrequest_' + oid, nRow).live('click', function () {
                $scope.$apply(function () {
                    sendRequest(userInformation, oid, 'Delete');
                });
            });
            
            $('#acceptrequest_' + oid, nRow).live('click', function () {
                $scope.$apply(function () {
                    sendRequest(userInformation, oid, 'Accept');
                    $.connection.hub.start().done(function () {

                        chat.server.send("ABCD", userInformation.FirstName+" accepted friend request");
                    });
                });
            });
        };

        var sendRequest = function (userInformation, oid, status) {
            userInformation.OperationType = 'FriendRequest';
            userInformation.Id = oid;
            userInformation.Status = status;
            //loadService.showDialog();
            promis = userService.addFriend(userInformation);
            promis.then(function (data) {
                if (!data.IsSuccess) {
                    loadService.hideDialog();
                    messageService.openErrorMessage(data.Code);
                    return;
                }
                messageService.openSuccessMessage(data.Code);
                $scope.reloadGrid();
                //loadService.hideDialog();
            });
        };

        

        $scope.myRowCallback = function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            debugger;
            var oid = nRow.cells[1].children[0].children[2].children[0].attributes['alt'].nodeValue;
            userInfo = cookiesService.getValue(constantService.userInfoCookieStoreKey);
            //constantService.clickCheckBoxOnGridRow(nRow);
            clickOnRow(userInfo, nRow, oid);
            return nRow;
        };

        $scope.myDrawCallback = function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            userInfo = cookiesService.getValue(configurationService.loginCookieStoreKey);
            //constantService.setCustomToolbar(userInfo);
            return nRow;
        };

        var load = function () {
            console.log("Pending Request Controller");
            userInfo = cookiesService.getValue(constantService.userInfoCookieStoreKey);
            $scope.overrideOptions = constantService.getOverrideOptions(configurationService.pendingUserDataGridUrl, userInfo);
            $scope.columnDefs = pendingRequestService.getColumnDefs(userInfo);
        };

        load();
        

        
}]);