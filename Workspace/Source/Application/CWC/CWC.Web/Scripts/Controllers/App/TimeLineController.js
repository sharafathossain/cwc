﻿
'use strict';

cwcApp.controller('timeLineController', ['$rootScope', '$scope', '$log', 'loadService', 'timeLineService',
    'messageService', 'cookiesService', 'constantService', 'configurationService',
    function ($rootScope, $scope, $log, loadService, timeLineService, messageService, cookiesService,
        constantService, configurationService) {

        var promis, userInfo;
        var chat = $.connection.notificationHub;
        
        $scope.saveStatus = function (statusObj) {
            if (statusObj == null) {
                return;
            }
            userInfo = cookiesService.getValue(constantService.userInfoCookieStoreKey);
            statusObj.OperationType = 'SaveStatus';
            statusObj.LoginViewModel = userInfo;
            promis = timeLineService.saveStatus(statusObj);
            promis.then(function (data) {
                if (!data.IsSuccess) {
                    loadService.hideDialog();
                    messageService.openErrorMessage("Unable to Save Post");
                    return;
                }
                messageService.openSuccessMessage(data.Code);
                $scope.reloadGrid();
                loadService.hideDialog();
            });
        };
        
        var clickOnRow = function (userInformation, nRow, oid) {
            $('#likecount_' + oid, nRow).live('click', function () {
                $scope.$apply(function () {
                    doLike(userInformation, oid, 'LikeCount');
                    chat.server.send("ABCD", userInformation.FirstName + " like post.");
                });
            });
            $('#removepost_' + oid, nRow).live('click', function () {
                $scope.$apply(function () {
                    doLike(userInformation, oid, 'Delete');
                });
            });
            $('#addcomment_' + oid, nRow).live('click', function () {
                $scope.$apply(function () {
                    var comment = $('#txtcomment_' + oid).val();
                    
                    $('#comment_' + oid).append('<p>' + comment + '</p>');
                    $('#txtcomment_' + oid).val('');
                });
            });
        };

        var doLike = function (userInformation, oid, status) {
            var obj = {};
            obj.Id = oid;
            obj.LoginViewModel = userInformation;
            obj.OperationType = status;
            //loadService.showDialog();
            promis = timeLineService.likeCount(obj);
            promis.then(function (data) {
                if (!data.IsSuccess) {
                    loadService.hideDialog();
                    messageService.openErrorMessage(data.Code);
                    return;
                }
                messageService.openSuccessMessage(data.Code);
                $scope.reloadGrid();
                //loadService.hideDialog();
            });
        };
        
        $scope.myRowCallback = function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            var oid = nRow.cells[0].children[0].children[0].children[2].children[0].attributes['alt'].nodeValue; 
            userInfo = cookiesService.getValue(constantService.userInfoCookieStoreKey);
            constantService.clickCheckBoxOnGridRow(nRow);
            clickOnRow(userInfo, nRow, oid);
            return nRow;
        };

        $scope.myDrawCallback = function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            return nRow;
        };

        var load = function () {
            userInfo = cookiesService.getValue(constantService.userInfoCookieStoreKey);
            $scope.overrideOptions = constantService.getTimeLineOverrideOptions(configurationService.timeLineDataGridUrl, userInfo);
            $scope.columnDefs = timeLineService.getColumnDefs(userInfo);
        };

        load();
        

        
}]);