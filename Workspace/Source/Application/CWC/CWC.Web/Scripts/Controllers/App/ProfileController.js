﻿
'use strict';

cwcApp.controller('profileController', ['$rootScope', '$scope', '$http', '$timeout', '$upload', '$log', 'loadService', 'questionService', 'messageService', 'constantService', 'cookiesService',
    function ($rootScope, $scope, $http, $timeout, $upload, $log, loadService, questionService, messageService, constantService, cookiesService) {

        $scope.upload = [];
        $scope.fileUploadObj = { LoginID: "", FirstName: "" };

        $scope.onFileSelect = function ($files) {
            $scope.fileUploadObj.LoginID = $scope.userInfo.LoginId;
            $scope.fileUploadObj.FirstName = $scope.userInfo.FirstName;

            //$files: an array of files selected, each file has name, size, and type.
            for (var i = 0; i < $files.length; i++) {
                var $file = $files[i];
                (function (index) {
                    $scope.upload[index] = $upload.upload({
                        url: "./api/files/upload", // webapi url
                        method: "POST",
                        data: { fileUploadObj: $scope.fileUploadObj },
                        file: $file
                    }).progress(function (evt) {
                        // get upload percentage
                        console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
                    }).success(function (data, status, headers, config) {
                        // file is uploaded successfully
                        //console.log('aaaa');
                        $scope.userInfo.PhotoPath = "../../Images/Imgs/" + $scope.userInfo.LoginId + ".png";
                        $rootScope.PhotoPath = $scope.userInfo.PhotoPath;
                        cookiesService.setValue(constantService.userInfoCookieStoreKey, $scope.userInfo);
                        $('div#loggedinright img').removeAttr('src');
                        $('div#loggedinright img').attr('src', $rootScope.PhotoPath);
                    }).error(function (data, status, headers, config) {
                        // file failed to upload
                        console.log(data);
                    });
                })(i);
            }
        }

        $scope.abortUpload = function (index) {
            $scope.upload[index].abort();
        }

        var load = function () {
            $('input[type=file]').bootstrapFileInput();
            $scope.userInfo = cookiesService.getValue(constantService.userInfoCookieStoreKey);
    };


    load();
        

        
}]);