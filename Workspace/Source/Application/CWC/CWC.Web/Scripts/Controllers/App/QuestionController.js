﻿
'use strict';

cwcApp.controller('questionController', ['$rootScope', '$scope', '$log', 'loadService', 'questionService', 'messageService',
    function ($rootScope, $scope, $log, loadService, questionService, messageService) {

        var promis;
        //$.connection.hub.logging = true;

        /*$.connection.hub.start()
            .done(function () {
                console.log("hub.start.done");
            })
            .fail(function (error) {
                console.log(error);
            });
        var hubProxy = $.connection.friendHub;*/
        

    $scope.saveQuestion = function (obj) {
        if (obj == null) {
            return;
        }
        obj.OperationType = 'SaveQuestion';
        loadService.showDialog();
        promis = questionService.saveQuestion(obj);
        promis.then(function (data) {
            if (!data.IsSuccess) {
                loadService.hideDialog();
                messageService.openErrorMessage("Unable to Save Question");
                return;
            }
            messageService.openSuccessMessage(data.Code);
            loadService.hideDialog();
        });
        
        //hubProxy.server.add(obj);

    };
        
    //hubProxy.client.add = function (friend) {
    //    console.log("client.add(" + JSON.stringify(friend) + ")");
    //    console.log(friend);
    //    $scope.$apply();
    //};
    
    var load = function () {
        //$log.info('Run Question Controller!!!');
    };


    load();
        

        
}]);