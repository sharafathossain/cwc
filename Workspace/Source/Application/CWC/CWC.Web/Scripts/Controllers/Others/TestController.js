﻿
'use strict';

cwcApp.controller('testController', ['$rootScope', '$scope', '$log', 'signalRHubProxy',
    function ($rootScope, $scope, $log, signalRHubProxy) {

    var clientPushHubProxy = signalRHubProxy(signalRHubProxy.defaultServer, 'clientPushHub', { logging: true });

    clientPushHubProxy.on('serverTime', function (data) {
        $scope.currentServerTime = data;
        var x = clientPushHubProxy.connection.id;
        $log.info(currentServerTime);
    });
        
    $scope.getData = function () {
        clientPushHubProxy.invoke('serverTime', function (data) {
            $scope.currentServerTime = data;
            $log.info(data);
        });
    };

    var load = function () {
        $log.info('Run Test Controller!!!');
    };


    load();
        

        
}]);