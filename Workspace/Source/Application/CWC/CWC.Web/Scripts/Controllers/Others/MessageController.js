﻿
'use strict';

cwcApp.controller('messageController', ['$rootScope', '$scope', 'constantService', 'messageService',
    function ($rootScope, $scope, constantService, messageService) {


    $scope.closeErrorMsg = function () {
        messageService.closeErrorMessage();
    };
        
    $scope.$on(constantService.ErrorMessage, function (event, errorMessageObj) {
        $scope.isErrorMsgVisible = errorMessageObj.isErrorMsgVisible;
        $scope.txtErrorMessage = errorMessageObj.txtErrorMessage;
        $scope.isSuccessMsgVisible = errorMessageObj.isSuccessMsgVisible;
        $scope.txtSuccessMessage = errorMessageObj.txtSuccessMessage;
    });

    var load = function () {
        $scope.isErrorMsgVisible = false;
        $scope.txtErrorMessage = constantService.DefaultCode;
        $scope.isSuccessMsgVisible = false;
        $scope.txtSuccessMessage = constantService.DefaultCode;
    };

    load();
        
}]);