
'use strict';

cwcApp.controller('userController', ['$rootScope', '$scope', 'userService',
    'navigationService', 'cookiesService', 'constantService', 'menuUtilsService', 'loadService', 'messageService',
    function ($rootScope, $scope, userService, navigationService,
    cookiesService, constantService, menuUtilsService, loadService, messageService) {
        
		var promis;
	 
        $scope.createNewUser = function (obj) {

            if (obj == null) {
                return;
            }
            obj.OperationType = 'CreateAccount';
            loadService.showDialog();
            promis = userService.createAccount(obj);
            promis.then(function (data) {
                if (!data.IsSuccess) {
                    loadService.hideDialog();
                    messageService.openErrorMessage(data.Code);
                    return;
                }
                messageService.openSuccessMessage(data.Code);
                loadService.hideDialog();
            });
        };
        

    }
]);