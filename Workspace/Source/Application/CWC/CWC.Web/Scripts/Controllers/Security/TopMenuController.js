﻿
'use strict';

cwcApp.controller('topMenuController', ['$rootScope', '$scope', 'cookiesService',
    'configurationService', 'navigationService', 'menuUtilsService', 'constantService',
    function ($rootScope, $scope, cookiesService, configurationService, navigationService, menuUtilsService, constantService) {

        $scope.clickAppTopMenu = function (topMenuId, $index) {
            var userInfo = cookiesService.getValue(constantService.userInfoCookieStoreKey);
            var appTopMenus = menuUtilsService.getTopMenuItems();

            $.each(appTopMenus, function (i, item) {
                if (item.url == topMenuId) {
                    userInfo.selectedTopMenu = topMenuId;
                    cookiesService.setValue(constantService.userInfoCookieStoreKey, userInfo);
                    $scope.selectedTopMenuIndex = $index;
                    $scope.selectedTopMenu = userInfo.selectedTopMenu;
                    navigationService.showPage(topMenuId);
                }
            });
        };
        
        $scope.show = function (pageName) {
            navigationService.showPage(pageName);
        };


        var load = function () {
            var userInfo = cookiesService.getValue(constantService.userInfoCookieStoreKey);
            $scope.appTopMenus = menuUtilsService.getTopMenuItems();
            $scope.selectedTopMenuIndex = 0;
            if (userInfo == null) {
                return;
            }
            $scope.selectedTopMenu = userInfo.selectedTopMenu;
        };

        load();

    }]);