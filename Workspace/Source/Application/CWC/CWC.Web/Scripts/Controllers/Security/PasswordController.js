
'use strict';

cwcApp.controller('passwordController', ['$rootScope', '$scope', 'passwordService',
    'navigationService', 'cookiesService', 'constantService', 'menuUtilsService', 'loadService', 'messageService',
    function ($rootScope, $scope, passwordService, navigationService,
    cookiesService, constantService, menuUtilsService, loadService, messageService) {
        
        var promis, userInfo;


        $scope.changePassword = function (obj) {
            if (obj == null) {
                return;
            }
            if (obj.Password != obj.ConfirmPassword) {
                return;
            }
            obj.OperationType = 'ChangePassword';
            userInfo = cookiesService.getValue(constantService.userInfoCookieStoreKey);
            obj.Email = userInfo.LoginId;
            loadService.showDialog();
            promis = passwordService.changePassword(obj);
            promis.then(function (data) {
                if (!data.IsSuccess) {
                    loadService.hideDialog();
                    messageService.openErrorMessage("Unable to Change Password");
                    return;
                }
                messageService.openSuccessMessage(data.Code);
                loadService.hideDialog();
            });
        };

	
		


    }
]);