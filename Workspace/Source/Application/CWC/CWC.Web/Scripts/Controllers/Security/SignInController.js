
'use strict';

cwcApp.controller('signinController', ['$rootScope', '$scope', 'signInService',
    'navigationService', 'cookiesService', 'constantService', 'menuUtilsService', 'loadService', 'messageService','googleService',
    function ($rootScope, $scope, signInService, navigationService,
    cookiesService, constantService, menuUtilsService, loadService, messageService, googleService) {
        
		var promis;
	
		$scope.userInfo = {
		    LoginId: '',
		    Password: ''
		};
        
		$scope.newUserInfo = {
		    Gender: 'Male'
		};
        
		$scope.signIn = function (obj) {
        	if(obj == null){
        		return;
        	}
        	obj.OperationType = 'SignIn';
        	loadService.showDialog();
        	promis = signInService.signin(obj);
            promis.then(function (data) {
                if (!data.IsSuccess) {
                    loadService.hideDialog();
                    messageService.openErrorMessage("Unable to Login");
                    return;
                }
                var firstTopMenuItem = menuUtilsService.getTopMenuItems()[0];
                data.Data.selectedTopMenu = firstTopMenuItem.url;
                cookiesService.setValue(constantService.userInfoCookieStoreKey, data.Data);
                //$rootScope.FullName = data.Data.FullName;
                //$rootScope.PhotoPath = data.Data.PhotoPath;
                navigationService.showPage(firstTopMenuItem.url);
                loadService.hideDialog();
            });
        };
        
		$scope.signOut = function () {
		    $scope.userInfo = {
		        LoginId: '',
		        Password: ''
		    };
		    $rootScope.userInfo = {
		        LoginId: '',
		        Password: ''
		    };
            cookiesService.setValue(constantService.userInfoCookieStoreKey,null);
            navigationService.showPage('');
        };
          
        $scope.confirm = function () {
            var firstTopMenuItem = menuUtilsService.getTopMenuItems()[0];
            navigationService.showPage(firstTopMenuItem.url);
        };

        
        
        
        $scope.$on('event:google-plus-signin-success', function (event, authResult) {
            // User successfully authorized the G+ App!
           var firstTopMenuItem = menuUtilsService.getTopMenuItems()[0];
           var obj = {};
            obj.selectedTopMenu=firstTopMenuItem.url;
            obj.LoginId = "mdjamal1984@gmail.com";
            cookiesService.setValue(constantService.userInfoCookieStoreKey, obj);
            navigationService.showPage(firstTopMenuItem.url);
           //document.getElementById("gLogin").click();
           console.log('Signed in!');
       });
        $scope.$on('event:google-plus-signin-failure', function (event, authResult) {
            // User has not authorized the G+ App!
            console.log('Not signed into Google Plus.');
        });

        $scope.login = function () {
            googleService.login().then(function (data) {
                var firstTopMenuItem = menuUtilsService.getTopMenuItems()[0];
                var obj = {};
                obj.selectedTopMenu = firstTopMenuItem.url;
                obj.LoginId = data.userdetails.email;
                obj.FirstName = data.userdetails.given_name;
                obj.LastName = data.userdetails.family_name;
                obj.Gender = data.userdetails.gender;
                obj.PhotoPath = data.userdetails.picture;
                obj.FullName = data.userdetails.name;
                cookiesService.setValue(constantService.userInfoCookieStoreKey, obj);
                navigationService.showPage(firstTopMenuItem.url);
                document.getElementById("gLogin").click();
                $rootScope.FullName = data.userdetails.name;
                $rootScope.PhotoPath = data.userdetails.picture;
                console.log(data.userdetails);
            }, function (err) {
                console.log('Failed: ' + err);
            });
        };

    }
]);