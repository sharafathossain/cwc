﻿
'use strict';

cwcApp.factory('signInService', ['$resource', 'configurationService', '$q', function ($resource,
    configurationService, $q) {

    var signInResource, delay, doSignIn, doSignOut, changePassword;
    
    signInResource = $resource(configurationService.signInWebApiUrl, { id: '@Id' }, {
        signin: { method: 'POST' },
        signout: { method: 'POST' },
        changePassword: { method: 'POST' }
    });
    
    doSignIn = function (obj) {
        delay = $q.defer();
        signInResource.signin(obj, function (data) {
            delay.resolve(data);
        }, function () {
            delay.reject('Unable to fetch..');
        });
        return delay.promise;
    };
    
    doSignOut = function (obj) {
        delay = $q.defer();
        signInResource.signout(obj, function (data) {
            delay.resolve(data);
        }, function () {
            delay.reject('Unable to fetch..');
        });
        return delay.promise;
    };
    
    changePassword = function (obj) {
        delay = $q.defer();
        signInResource.changePassword(obj, function (data) {
            delay.resolve(data);
        }, function () {
            delay.reject('Unable to fetch..');
        });
        return delay.promise;
    };
    
   

    return {
        signin: doSignIn, signOut: doSignOut, changePassword: changePassword
    };
    
}]);