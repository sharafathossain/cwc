﻿
'use strict';

cwcApp.factory('srvAuth', ['$resource', 'configurationService', '$q', 'navigationService', 'cookiesService', 'constantService', 'menuUtilsService', function ($resource,
    configurationService, $q, navigationService,
    cookiesService, constantService, menuUtilsService) {

    var watchLoginChange, getUserInfo;

    watchLoginChange = function () {

        var _self = this;

        FB.Event.subscribe('auth.authResponseChange', function (response) {

            console.log(response);

            if (response.status === 'connected') {
                debugger;
                var firstTopMenuItem = menuUtilsService.getTopMenuItems()[0];
                //data.Data.selectedTopMenu = firstTopMenuItem.url;
                //cookiesService.setValue(constantService.userInfoCookieStoreKey, data.Data);
                navigationService.showPage(firstTopMenuItem.url);

                /* 
                 The user is already logged, 
                 is possible retrieve his personal info
                */
                //getUserInfo();

                /*
                 This is also the point where you should create a 
                 session for the current user.
                 For this purpose you can use the data inside the 
                 response.authResponse object.
                */

            }
            else {

                /*
                 The user is not logged to the app, or into Facebook:
                 destroy the session on the server.
                */

            }

        });

    }

    getUserInfo = function () {

        console.log('Welcome!  Fetching your information.... ');
        FB.api('/me', function (response) {
            console.log(response);
            console.log('Successful login for: ' + response.name);
           // document.getElementById('status').innerHTML = 'Thanks for logging in, ' + response.name + '!';
        });
    };


    return {
        watchLoginChange: watchLoginChange, getUserInfo: getUserInfo
    };
    
}]);