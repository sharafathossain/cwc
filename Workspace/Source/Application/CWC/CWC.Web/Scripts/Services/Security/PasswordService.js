﻿
'use strict';

cwcApp.factory('passwordService', ['$resource', 'configurationService', '$q', function ($resource,
    configurationService, $q) {

    var passwordResource, delay, changePassword;
    
    passwordResource = $resource(configurationService.userWebApiUrl, { id: '@Id' }, {
        changePassword: { method: 'POST' }
    });
    
    
    changePassword = function (obj) {
        delay = $q.defer();
        passwordResource.changePassword(obj, function (data) {
            delay.resolve(data);
        }, function () {
            delay.reject('Unable to fetch..');
        });
        return delay.promise;
    };

    return {
        changePassword: changePassword
    };
    
}]);