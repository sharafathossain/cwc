﻿
'use strict';

cwcApp.factory('messageService', ['$rootScope', 'constantService',
    function ($rootScope, constantService) {

        var openErrorMessage, closeErrorMessage, openSuccessMessage;

        closeErrorMessage = function () {
            var errorMessageObj = {};
            errorMessageObj.isErrorMsgVisible = false;
            errorMessageObj.isSuccessMsgVisible = false;
            errorMessageObj.txtErrorMessage = constantService.DefaultCode;
            errorMessageObj.txtSuccessMessage = constantService.DefaultCode;
            $rootScope.$broadcast(constantService.ErrorMessage, errorMessageObj);
        };
        
        openErrorMessage = function (errorText) {
            var errorMessageObj = { };
            errorMessageObj.isErrorMsgVisible = true;
            errorMessageObj.isSuccessMsgVisible = false;
            errorMessageObj.txtErrorMessage = errorText;
            errorMessageObj.txtSuccessMessage = constantService.DefaultCode;
            $rootScope.$broadcast(constantService.ErrorMessage, errorMessageObj);
        };
        
        openSuccessMessage = function (successText) {
            var errorMessageObj = {};
            errorMessageObj.isErrorMsgVisible = false;
            errorMessageObj.isSuccessMsgVisible = true;
            errorMessageObj.txtErrorMessage = constantService.DefaultCode;
            errorMessageObj.txtSuccessMessage = successText;
            $rootScope.$broadcast(constantService.ErrorMessage, errorMessageObj);
        };
        

        return {
            closeErrorMessage: closeErrorMessage, openErrorMessage: openErrorMessage, openSuccessMessage: openSuccessMessage
        };

}]);