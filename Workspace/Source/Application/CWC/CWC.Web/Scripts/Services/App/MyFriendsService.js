﻿
'use strict';

cwcApp.factory('myFriendsService', ['$resource', 'configurationService', '$q', function ($resource,
    configurationService, $q) {

    var userResource, delay, createAccount, getColumnDefs, addFriend;
    
    userResource = $resource(configurationService.userWebApiUrl, { id: '@id' }, {
        createAccount: { method: 'POST' },
        addFriend: { method: 'POST' }
    });
   
    
    createAccount = function (obj) {
        delay = $q.defer();
        userResource.createAccount(obj, function (data) {
            delay.resolve(data);
        }, function () {
            delay.reject('Unable to fetch..');
        });
        return delay.promise;
    };
    
    addFriend = function (obj) {
        delay = $q.defer();
        userResource.addFriend(obj, function (data) {
            delay.resolve(data);
        }, function () {
            delay.reject('Unable to fetch..');
        });
        return delay.promise;
    };
    
    getColumnDefs = function (userInfo) {
        var columnDefs = [
            {
                "mDataProp": "Gender",
                //"sTitle": "<div><a id=\"selectall_checkbox\" class=\"jqTransformCheckbox\" onclick=\"toggleAll()\"></a></div>",
                "sTitle": "",
                "aTargets": [0],
                "sWidth": "20%",
                "bSearchable": false,
                "bSortable": false,
                "fnRender": function (obj) {
                    var id = obj.aData["Email"];
                    var firstName = obj.aData["FirstName"];
                    var lastName = obj.aData["LastName"];
                    var photoPath = obj.aData["PhotoPath"];

                    

                    
                    var sReturn = "<div data-toggle=\"modal\" data-target=\"#4567\" class=\"rowElem friendsElem\" id=\"" + id + "\"><span style=\"display:none;\">" + id + "</span><img class=\"findFriendsImage\" src=\"" + photoPath + "\" title=\"userPhoto\" alt=\"userPhoto\"/></div>"
                        + "<div class=\"modal fade\" id=\"4567\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">"
                            + "<div class=\"modal-dialog\">"
                                + "<div class=\"modal-content\">"
                                    + "<div class=\"modal-body\">"
                                        + "<center>"
                                        + "<img src=\"" + photoPath + "\" name=\"aboutme\" width=\"140\" height=\"140\" border=\"0\" class=\"img-circle\"></a>"
                                         + "<h3 class=\"media-heading\">" + firstName + " <small>" + lastName + "</small></h3>"
                                            + "<span class=\"label label-warning\">" + id + "</span>"
                                        + "</center>"
                                    + "</div>"
                                    + "<div class=\"modal-footer\">"
                                        + "<center>"
                                        + "<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">I have read information</button>"
                                        + "</center>"
                                    + "</div>"
                                + "</div>"
                            + "</div>"
                        + "</div>";
                    return sReturn;
                }
            },
            {
                "mDataProp": "OperationType",
                "sTitle": "",
                "sType": "html",
                "aTargets": [1],
                "bSearchable": true,
                "bSortable": false,
                "fnRender": function (obj) {
                    var firstName = obj.aData["FirstName"];
                    var lastName = obj.aData["LastName"];
                    var id = obj.aData["Id"];
                    var status = obj.aData["Status"];
                    var friendId = obj.aData["FriendId"];
                    var userId = obj.aData["UserId"];
                    

                    var myDiv =  "<div class=\"friendsInfo\"><p>" + firstName + "</p><p>" + lastName +
                        "</p><p>";
                    
                    if (status != null && status == 'Pending' && userInfo.LoginId == userId)
                    {
                        myDiv += "<a class=\"link\" id=\"cancelrequest_" + id + "\" alt=\"" + id + "\">Cancel</a>";
                    }
                    else if (status != null && status == 'Pending' && userInfo.LoginId == friendId)
                    {
                        myDiv += "<a class=\"link\" id=\"acceptrequest_" + id + "\" alt=\"" + id + "\">Accept</a>";
                        myDiv += " | <a class=\"link\" id=\"cancelrequest_" + id + "\" alt=\"" + id + "\">Deny</a>";
                    }
                    else if (status != null && status == 'Accept') {
                        myDiv += " Friend | <a class=\"link\" id=\"cancelrequest_" + id + "\" alt=\"" + id + "\">UnFriend</a>";
                    }
                    else {
                        myDiv += "<a class=\"link\" id=\"sendrequest_" + id + "\" alt=\"" + id + "\">Send</a>";
                    }
                    myDiv += "</p></div>";
                    return myDiv;
                }
            }
        ];
        return columnDefs;
    };

    return {
        createAccount: createAccount, getColumnDefs: getColumnDefs, addFriend: addFriend
    };
    
}]);