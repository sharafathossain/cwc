﻿
'use strict';

cwcApp.factory('questionService', ['$resource', 'configurationService', '$q',
    function ($resource, configurationService, $q) {

    var questionResource, delay, saveQuestion;
    
    questionResource = $resource(configurationService.questionWebApiUrl, { id: '@Id' }, {
        saveQuestion: { method: 'POST' }
    });
    
    saveQuestion  = function (obj) {
        delay = $q.defer();
        questionResource.saveQuestion(obj, function (data) {
            delay.resolve(data);
        }, function () {
            delay.reject('Unable to fetch..');
        });
        return delay.promise;
    };
    
    

    return {
        saveQuestion: saveQuestion
    };
    
}]);