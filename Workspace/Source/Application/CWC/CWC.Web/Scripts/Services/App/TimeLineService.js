﻿
'use strict';

cwcApp.factory('timeLineService', ['$resource', 'configurationService', '$q',
    function ($resource, configurationService, $q) {

        var homeResource, delay, saveStatus, getColumnDefs, likeCount;
    
    homeResource = $resource(configurationService.homeWebApiUrl, { id: '@Id' }, {
        saveStatus: { method: 'POST' },
        likeCount: { method: 'POST' }
    });
    
    saveStatus = function (obj) {
        delay = $q.defer();
        homeResource.saveStatus(obj, function (data) {
            delay.resolve(data);
        }, function () {
            delay.reject('Unable to fetch..');
        });
        return delay.promise;
    };
        
    likeCount = function (obj) {
        delay = $q.defer();
        homeResource.likeCount(obj, function (data) {
            delay.resolve(data);
        }, function () {
            delay.reject('Unable to fetch..');
        });
        return delay.promise;
    };

    getColumnDefs = function (userInfo) {
        var columnDefs = [
            {
                "mDataProp": "Status",
                "sTitle": "",
                "aTargets": [0],
                "bSearchable": false,
                "bSortable": false,
                "fnRender": function (obj) {
                    

                    var status = obj.aData["Status"];
                    var like = obj.aData["Like"];
                    var id = obj.aData["Id"];
                    var userId = obj.aData["UserId"];
                    var inputArray = obj.aData["LikeUsers"];
                    
                    var outputArray = [];
                    for (var i = 0; i < inputArray.length; i++) {
                        if ((jQuery.inArray(inputArray[i], outputArray)) == -1) {
                            outputArray.push(inputArray[i]);
                        }
                    }

                    outputArray = outputArray.join(', ');
                    var myDiv = " <div class=\"informationThumbnail\">" +
    				
					" <div class=\"infoDiv\">" +
        					" <div class = \"status\">" + status + "</div>&nbsp;&nbsp;&nbsp; <div class = \"like\">Like : " + like + " </div>" +
        					" <p><a class=\"link\" id=\"likecount_" + id + "\" alt=\"" + id + "\"> Like </a>";
                    if (userId == userInfo.LoginId)
                    {
                        myDiv += " | <a class=\"link\" id=\"removepost_" + id + "\" alt=\"" + id + "\"> Remove </a>";
                    }
                    myDiv += "</p><p>Friends : " + outputArray + "</p><div id=\"comment_" + id + "\"></div><p><input type=\"text\" " +
                        "style=\"height:35px;padding: 6px 12px;\" placeholder=\"Enter Comment\" id=\"txtcomment_" + id + "\"/>" +
                        "<button id=\"addcomment_" + id + "\" class=\"btn btn-primary\" style=\"margin: 0 0 12px 7px;\">Add</button></p></div> </div>";
                    return myDiv;

                }
            }
        ];
        return columnDefs;
    };

    return {
        saveStatus: saveStatus, getColumnDefs: getColumnDefs, likeCount: likeCount
    };
    
}]);