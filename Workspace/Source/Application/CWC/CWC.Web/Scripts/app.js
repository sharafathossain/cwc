﻿
'use strict';

var cwcApp = angular.module('cwcApp', ['localization', 'ngSanitize', 'ngResource', 'ngCookies', 'ui', 'ngGrid', 'ui.bootstrap', 'directive.g+signin', 'angularFileUpload']);
cwcApp.value('signalRServer', '');

cwcApp.run(['$rootScope', '$route', 'localize', '$log', 'cookiesService', 'constantService', 'navigationService',
    function ($rootScope, $route, localize, $log, cookiesService, constantService, navigationService) {

    localize.setLanguage('en-US');
    $rootScope.messagePageLocation = 'Views/Others/Message.html';
    
    $rootScope.partial = {
        header: { location: 'Views/Layout/Header.html', isVisible: false },
        footer: { location: 'Views/Layout/Footer.html', isVisible: false },
        headerLogin: { location: 'Views/Layout/HeaderLogin.html', isVisible: false },
    };
    
    $rootScope.$on('$routeChangeSuccess', function () {
        $rootScope.pageTitle = $route.current.$$route.title;
    });
    
    $rootScope.$on("$routeChangeStart", function (oldPath, newPath) {
        var userInfo = cookiesService.getValue(constantService.userInfoCookieStoreKey);
        if (userInfo == null || userInfo == undefined) {
            $rootScope.partial.header.isVisible = false;
            $rootScope.partial.footer.isVisible = false;
            navigationService.showPage('');
            return;
        }
        else {
            if (newPath.$$route !== undefined && !newPath.$$route.isWeb) {
                $rootScope.partial.header.isVisible = true;
                $rootScope.partial.footer.isVisible = true;
            }
            else {
                $rootScope.partial.header.isVisible = false;
                $rootScope.partial.footer.isVisible = false;
            }
            $rootScope.PhotoPath = userInfo.PhotoPath;
            $rootScope.FullName = userInfo.FullName;
        }
    });

}]);

cwcApp.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.
        when('/', { templateUrl: 'Views/Security/Login.html', controller: 'signinController', title: 'Login', isWeb: true }).
        when('/home', { templateUrl: 'Views/App/Home.html', controller: 'timeLineController', title: 'BitBook', isWeb: false }).
        when('/showprofile', { templateUrl: 'Views/App/Profile.html', controller: 'profileController', title: 'Profile', isWeb: false }).
        when('/password', { templateUrl: 'Views/Security/Password.html', controller: 'passwordController', title: 'Password', isWeb: false }).
        when('/findfriends', { templateUrl: 'Views/App/FindFriends.html', controller: 'findFriendController', title: 'Find Friends', isWeb: false }).
        when('/pendingrequests', { templateUrl: 'Views/App/PendingRequests.html', controller: 'pendingRequestController', title: 'Pending Requests', isWeb: false }).
        when('/friends', { templateUrl: 'Views/App/MyFriends.html', controller: 'myFriendsController', title: 'My Friends', isWeb: false }).
        when('/question', { templateUrl: 'Views/App/Question.html', controller: 'questionController', title: 'Question', isWeb: false }).
        otherwise({ redirectTo: '/' });
}]);

    function toggleAll() {
        var className = $("#selectall_checkbox").attr("class");
        if (className == "jqTransformCheckbox jqTransformChecked") {
            $("#selectall_checkbox").removeClass("jqTransformChecked");
            $('table.display tbody tr').each(function () {
                var cName = $(this).find("td").eq(0).find("div span a").attr("class");
                var studentid = $(this).find("td").eq(0).find("div span input").val();
                if (cName == "jqTransformCheckbox jqTransformChecked") {
                    $("#" + studentid + "_checkbox").find("span a").removeClass("jqTransformChecked");
                }
            });
        }
        else {
            $("#selectall_checkbox").addClass("jqTransformChecked");
            $('table.display tbody tr').each(function () {
                var cName = $(this).find("td").eq(0).find("div span a").attr("class");
                var studentid = $(this).find("td").eq(0).find("div span input").val();
                if (cName == "jqTransformCheckbox") {
                    $("#" + studentid + "_checkbox").find("span a").addClass("jqTransformChecked");
                }
            });
        }
    }