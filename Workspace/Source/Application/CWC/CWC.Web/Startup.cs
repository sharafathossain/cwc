﻿using Microsoft.Owin;
using Owin;
[assembly: OwinStartup(typeof(CWC.Web.Startup))]
namespace CWC.Web
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // Any connection or hub wire up and configuration should go here
            app.MapSignalR();
        }
    }
}