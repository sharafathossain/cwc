﻿
using System;
using System.Linq;
using System.Web.Http;
using System.Web.Mvc;
using Castle.Windsor;
using System.Web.Optimization;
using System.Web.Routing;
using System.Globalization;
using Castle.Windsor.Installer;
using CWC.Web.App_Start;
using CWC.Web.Windsor;
using Newtonsoft.Json;
using log4net.Config;

namespace CWC.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class WebApiApplication : System.Web.HttpApplication
    {
        private readonly WindsorContainer _container;

        public WebApiApplication()
        {
            _container = new WindsorContainer();
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RegisterDependencyResolver();
            InstallDependencies();
            XmlConfigurator.Configure();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            SerializeSettings(GlobalConfiguration.Configuration);
            RegisterApis(GlobalConfiguration.Configuration);
        }

        private void InstallDependencies()
        {
            _container.Install(FromAssembly.This());
        }
        private void RegisterDependencyResolver()
        {
            GlobalConfiguration.Configuration.DependencyResolver = new WindsorDependencyResolver(_container.Kernel);
        }

        protected void Application_End(object sender, EventArgs e)
        {
            if (_container != null)
                _container.Dispose();
        }
        private void SerializeSettings(HttpConfiguration config)
        {
            var jsonSetting = new JsonSerializerSettings();
            jsonSetting.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
            config.Formatters.JsonFormatter.SerializerSettings = jsonSetting;
        }

        private void RegisterApis(HttpConfiguration config)
        {
            // remove default Xml handler
            var matches = config.Formatters
                                .Where(f => f.SupportedMediaTypes.Any(m => m.MediaType.ToString(CultureInfo.InvariantCulture) == "application/xml" ||
                                                                           m.MediaType.ToString(CultureInfo.InvariantCulture) == "text/xml"))
                                .ToList();
            foreach (var match in matches)
                config.Formatters.Remove(match);
        }
    }
}