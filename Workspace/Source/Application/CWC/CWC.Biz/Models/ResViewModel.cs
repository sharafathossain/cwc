﻿
using CWC.Biz.Utils;

namespace CWC.Biz.Models
{
    public class ResViewModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
        public Code Code { get; set; }

        public ResViewModel()
        {
            IsSuccess = false;
            Message = "";
            Code = Code.Df1000;
        }

        public ResViewModel(bool isSuccess, string message, object data)
        {
            IsSuccess = isSuccess;
            Message = message;
            Data = data;
        }
        
    }
}
