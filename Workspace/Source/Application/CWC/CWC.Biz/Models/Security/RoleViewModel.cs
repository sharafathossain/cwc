﻿
namespace CWC.Biz.Models.Security
{
    public class RoleViewModel
    {
        public string RoleId { get; set; }
        public string RoleDescription { get; set; }
    }
}
