﻿
namespace CWC.Biz.Models.Security
{
    public class SignInViewModel
    {
        public string LoginId { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string RoleId { get; set; }
        public string PhotoPath { get; set; }
        public string Gender { get; set; }
        public RoleViewModel RoleViewModel { get; set; }
        public string OperationType { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }

    }
}
