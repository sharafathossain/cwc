﻿
using System.Collections.Generic;
using CWC.Biz.Models.Security;
using MongoDB.Bson;

namespace CWC.Biz.Models.App
{
    public class TimeLineViewModel
    {
        public string TimeLineId { get; set; }

        public string Status { get; set; }

        public long Like { get; set; }

        public string Id { get; set; }

        public string UserId { get; set; }

        public string ContentPath { get; set; }

        public LoginViewModel LoginViewModel { get; set; }

        public string OperationType { get; set; }

        public IList<string> LikeUsers { get; set; }
        

    }
}
