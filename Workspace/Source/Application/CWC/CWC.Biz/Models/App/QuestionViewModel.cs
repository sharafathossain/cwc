﻿
using System.Collections.Generic;
using CWC.Biz.Models.Security;

namespace CWC.Biz.Models.App
{
    public class QuestionViewModel
    {
        public string QuestionId { get; set; }
        public string QuestionDescription { get; set; }
        public ICollection<CommentViewModel> Comments { get; set; }
        public LoginViewModel LoginViewModel { get; set; }
        public string OperationType { get; set; }

    }
}
