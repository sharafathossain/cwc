﻿
using CWC.Biz.Models.Security;

namespace CWC.Biz.Models.App
{
    public class CommentViewModel
    {
        public string CommentId { get; set; }
        public string CommentDescription { get; set; }
        public LoginViewModel LoginViewModel { get; set; }
        public string OperationType { get; set; }

    }
}
