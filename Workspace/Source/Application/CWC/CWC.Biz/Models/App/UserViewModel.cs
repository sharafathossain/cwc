﻿

using System;
using CWC.Biz.Models.Security;

namespace CWC.Biz.Models.App
{
    public class UserViewModel
    {
        public string Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime DateOfBirth { get; set; }

        public string Gender { get; set; }

        public string Password { get; set; }

        public string PhotoPath { get; set; }

        public string Email { get; set; }

        public string LoginId { get; set; }

        public string Status { get; set; }

        public string FriendId { get; set; }

        public string UserId { get; set; }

        public string OperationType { get; set; }

    }
}
