﻿
using System;
using CWC.Biz.Models.App;
using CWC.Data.Entity;

namespace CWC.Biz.Converter
{
    public class ViewToEntityModel
    {
        
        public static Question ConvertQuestionViewToEntity(QuestionViewModel model)
        {
            var question = new Question();
            question.QuestionDescription = model.QuestionDescription;
            return question;
        }

        public static User ConvertUserViewToEntity(UserViewModel model)
        {
            var user = new User();
            user.Id = model.Id;
            user.FirstName = model.FirstName;
            user.LastName = model.LastName;
            user.DateOfBirth = model.DateOfBirth;
            user.Email = model.Email;
            user.Gender = model.Gender;
            user.Password = model.Password;
            user.PhotoPath = "../../Images/Imgs/" + model.Email + ".png";
            return user;
        }

        public static TimeLine ConvertTimeLineViewToEntity(TimeLineViewModel model)
        {
            var timeLine = new TimeLine();
            timeLine.Id = model.Id;
            timeLine.Status = model.Status;
            timeLine.UserId = model.LoginViewModel.LoginId;
            timeLine.CreatedOn = DateTime.Now;
            return timeLine;
        }


    }
}
