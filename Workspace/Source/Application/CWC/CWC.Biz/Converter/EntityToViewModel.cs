﻿
using CWC.Biz.Models.App;
using CWC.Biz.Models.Security;
using CWC.Data.Entity;

namespace CWC.Biz.Converter
{
    public class EntityToViewModel
    {

        public static SignInViewModel ConvertUserEntityToModel(User entity)
        {
            var model = new SignInViewModel();
            model.LoginId = entity.Email;
            model.FirstName = entity.FirstName;
            model.LastName = entity.LastName;
            model.PhotoPath = entity.PhotoPath;
            model.Gender = entity.Gender;
            model.FullName = entity.FirstName + " " + entity.LastName;
            return model;
        }


        public static UserViewModel ConvertUserEntityToViewModel(User entity)
        {
            var user = new UserViewModel();
            user.FirstName = entity.FirstName;
            user.LastName = entity.LastName;
            user.DateOfBirth = entity.DateOfBirth;
            user.Email = entity.Email;
            user.Gender = entity.Gender;
            user.Password = entity.Password;
            user.PhotoPath = entity.PhotoPath;
            user.Id = entity.Id;
            user.LoginId = entity.Email;
            return user;
        }

        public static TimeLineViewModel ConvertTimeLineEntityToViewModel(TimeLine entity)
        {
            var timeLineViewModel = new TimeLineViewModel();
            timeLineViewModel.TimeLineId = entity.TimeLineId.ToString();
            timeLineViewModel.Id = entity.Id;
            timeLineViewModel.UserId = entity.UserId;
            timeLineViewModel.Like = entity.Like;
            timeLineViewModel.Status = entity.Status;
            timeLineViewModel.LikeUsers = entity.LikeUsers;
            return timeLineViewModel;
        }

    }
}
