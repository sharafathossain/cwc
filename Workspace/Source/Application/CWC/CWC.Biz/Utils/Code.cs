﻿

namespace CWC.Biz.Utils
{
    public enum Code
    {
        Df1000,      // Default or Blank Text
        Sc1000,      // Successfully Save Data
        Sd1000,      // Successfully Delete Data
        Ud1000,      // Unable to Delete Data
        Ed1000,      // Already Exist Data
        Us1000,      // Unable to Save
        Uc1000,      // Unable to converter model to entity
        Nd1000,      // No Data Found
        Up1000,      // Unable to Update
    }
}
