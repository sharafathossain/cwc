﻿


using CWC.Biz.Models.Security;

namespace CWC.Biz.Utils
{
    public class DataTableParamModel
    {
        public int sEcho { get; set; }
        public int iColumns { get; set; }
        public int sColumns { get; set; }
        public int iDisplayStart { get; set; }
        public int iDisplayLength { get; set; }
        public int iSortCol_0 { get; set; }
        public string sSortDir_0 { get; set; }
        public string sSearch { get; set; }
        public bool bRegex { get; set; }
        public int iSortingCols { get; set; }
        public int iSortColumnIndex { get; set; }
        public string sSortDirection { get; set; }
        public LoginViewModel LoginViewModel { get; set; }
        
    }

}
