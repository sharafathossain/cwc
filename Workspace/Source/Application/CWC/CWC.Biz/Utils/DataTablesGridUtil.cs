﻿using System.Collections.Generic;

namespace CWC.Biz.Utils
{
    class DataTablesGridUtil
    {
        public static DataTableResModel GetResDataTable(DataTableParamModel paramModel, long iTotalRecords,
            long iTotalDisplayRecords, IEnumerable<object> modelList)
        {
            var resDataTable = new DataTableResModel();
            if (modelList == null)
            {
                return resDataTable;
            }
            resDataTable.sEcho = paramModel.sEcho;
            resDataTable.iTotalRecords = iTotalRecords;
            resDataTable.iTotalDisplayRecords = iTotalDisplayRecords;
            resDataTable.aaData = modelList;
            return resDataTable;
        }
    }
}
