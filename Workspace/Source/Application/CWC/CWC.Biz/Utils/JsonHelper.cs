﻿
using System;
using Castle.Core.Logging;
using Newtonsoft.Json;

namespace CWC.Biz.Utils
{
    public class JsonHelper
    {
        public static ILogger Log = NullLogger.Instance;

        public ILogger Logger
        {
            get { return Log; }
            set { Log = value; }
        }
        
        public static string ObjectToJson(object o)
        {
            try
            {
                return JsonConvert.SerializeObject(o);
            }
            catch (Exception ex)
            {
                Log.Error("An Exception occured while parse an object to json string : ", ex);
            }
            return null;
        }

        public static T JsonToObject<T>(string json) where T : class
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(json);
            }
            catch (Exception ex)
            {
                Log.Error("An Exception occured while parse json string to an Ojbect: ", ex);
            }
            return null;
        }
    }
}
