﻿
using System.Collections.Generic;

namespace CWC.Biz.Utils
{
    public class DataTableResModel
    {

        public int sEcho { get; set; }
        public long iTotalRecords { get; set; }
        public long iTotalDisplayRecords { get; set; }
        public object aaData { get; set; }

        public DataTableResModel()
        {
            sEcho = 1;
            iTotalRecords = 0;
            iTotalDisplayRecords = 0;
            aaData = new List<object>();
        }

        public DataTableResModel(int sEcho, long iTotalRecords, long iTotalDisplayRecords, object aaData)
        {
            this.sEcho = sEcho;
            this.iTotalRecords = iTotalRecords;
            this.iTotalDisplayRecords = iTotalDisplayRecords;
            this.aaData = aaData;
        }


    }
}
