﻿
using System;

namespace CWC.Biz.Utils
{
    public class IdGenerator
    {
        
        public static string GenerateOid()
        {
            string n = Guid.NewGuid().ToString("N");
            DateTime d = DateTime.Now;
            return String.Format("{0}{1}", d.ToString("yyyyMMddHHmmss"), Convert.ToString(n).Substring(0, 16).PadLeft(7, '0'));
        }

    }

}
