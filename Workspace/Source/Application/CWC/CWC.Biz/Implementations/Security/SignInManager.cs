﻿
using System;
using CWC.Biz.Contracts.Security;
using CWC.Biz.Converter;
using CWC.Biz.Models;
using CWC.Biz.Models.Security;
using CWC.Data.Entity;
using CWC.Lib.Repositories.Contarcts;
using MongoDB.Driver.Builders;
using System.Collections.Generic;

namespace CWC.Biz.Implementations.Security
{
    public class SignInManager : ManagerBase, ISignInManager
    {
        
        private readonly IRepository<User> _userRepository;

        public SignInManager(IRepository<User> userRepository)
            : base()
        {
            _userRepository = userRepository;
        }

        public void CreateUserDummyData()
        {

            /*for (int i = 1; i <= 3; i++ )
            {
                var user = new User();
                user.FirstName = "FirstName" + i;
                user.LastName = "LastName"+i;
                user.Email = user.FirstName.ToLower() + "@spectrum-bd.com";
                user.Password = "user"+i;
                user.Gender = "Male";
                user.Id = i.ToString();
                user.PhotoPath = "../../Images/Imgs/maleuser.jpg";

                _userRepository.GetCollection().Save(user);
            }
            Assert.AreEqual(3, _userRepository.GetCollection().Count()); */
            foreach (var user in GetUsers())
            {
                _userRepository.GetCollection().Save(user);
            }
          
        }
        private IEnumerable<User> GetUsers()
        {
            IList<User> users = new List<User>();
            var user1 = new User();
            user1.FirstName = "Jamal";
            user1.LastName = "Uddin";
            user1.Email = "jamal@spectrum-bd.com";
            user1.Password = "123";
            user1.Gender = "Male";
            user1.Id = "jamal";
            user1.PhotoPath = "../../Images/Imgs/jamal@spectrum-bd.com.png";
            users.Add(user1);

            var user2 = new User();
            user2.FirstName = "Kamruzzaman";
            user2.LastName = "Tanim";
            user2.Email = "ktanim@spectrum-bd.com";
            user2.Password = "123";
            user2.Gender = "Male";
            user2.Id = "ktanim";
            user2.PhotoPath = "../../Images/Imgs/ktanim@spectrum-bd.com.png";
            users.Add(user2);

            var user3 = new User();
            user3.FirstName = "Sharafat";
            user3.LastName = "hossain";
            user3.Email = "sharafathossain@spectrum-bd.com";
            user3.Password = "123";
            user3.Gender = "Male";
            user3.Id = "sharafat";
            user3.PhotoPath = "../../Images/Imgs/sharafathossain@spectrum-bd.com.png";
            users.Add(user3);

            var user4 = new User();
            user4.FirstName = "Kowsar";
            user4.LastName = "Bhuyian";
            user4.Email = "kowsar@spectrum-bd.com";
            user4.Password = "123";
            user4.Gender = "Male";
            user4.Id = "kowsar";
            user4.PhotoPath = "../../Images/Imgs/kowsar@spectrum-bd.com.png";
            users.Add(user4);

            return users;
        }

        public ResViewModel SignIn(SignInViewModel model)
        {
            //CreateUserDummyData();
            var resModel = new ResViewModel();
            try
            {

                var query = Query.And(
                    Query.EQ("Email", model.LoginId),
                    Query.EQ("Password", model.Password)
                );

                var loginEntity = _userRepository.GetCollection().FindOne(query);
                if(loginEntity == null)
                {
                    return resModel;
                }
                model = EntityToViewModel.ConvertUserEntityToModel(loginEntity);
                resModel.IsSuccess = true;
                resModel.Data = model;
                Log.Info("Successfully Signin By : " + model.LoginId);
            }
            catch (Exception ex)
            {
                Log.Error("An Exception occured while an user to try for signin : ", ex);
                resModel.IsSuccess = false;
            }
            return resModel;
        }

        public ResViewModel SignOut(SignInViewModel req)
        {
            var resModel = new ResViewModel();
            resModel.IsSuccess = true;
            return resModel;
        }
    }
}
