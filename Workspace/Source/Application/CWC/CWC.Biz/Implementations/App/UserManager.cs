﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using CWC.Biz.Contracts.App;
using CWC.Biz.Converter;
using CWC.Biz.Models;
using CWC.Biz.Models.App;
using CWC.Biz.Utils;
using CWC.Data.Entity;
using CWC.Lib.Repositories.Contarcts;
using MongoDB.Bson;
using MongoDB.Driver.Builders;

namespace CWC.Biz.Implementations.App
{
    public class UserManager : ManagerBase, IUserManager
    {

        private readonly IRepository<User> _userRepository;
        private readonly IRepository<Friend> _friendRepository;

        public UserManager(IRepository<User> userRepository, IRepository<Friend> friendRepository)
            : base()
        {
            _userRepository = userRepository;
            _friendRepository = friendRepository;
        }

        public ResViewModel CreateUserAccount(ResViewModel resViewModel, UserViewModel model)
        {
            model.Id = IdGenerator.GenerateOid();
            var user = ViewToEntityModel.ConvertUserViewToEntity(model);
            _userRepository.GetCollection().Save(user);
            var photoPath = ConfigurationManager.AppSettings["PhotoPath"];
            if (model.Gender.Equals("Male"))
            {
                File.Copy(photoPath + "maleuser.png", photoPath + user.Email + ".png", true);
            }
            else
            {
                File.Copy(photoPath + "femaleuser.png", photoPath + user.Email + ".png", true);
            }
            
            resViewModel.IsSuccess = true;
            resViewModel.Code = Code.Sc1000;
            return resViewModel;
        }

        public ResViewModel ChangePassword(ResViewModel resViewModel, UserViewModel model)
        {
            var query = Query.And(
                Query.EQ("Email", model.Email)
                );

            var userEntity = _userRepository.GetCollection().FindOne(query);
            if (userEntity == null)
            {
                return resViewModel;
            }
            userEntity.Password = model.Password;
            _userRepository.GetCollection().Save(userEntity);
            resViewModel.IsSuccess = true;
            resViewModel.Code = Code.Sc1000;
            return resViewModel;
        }

        public DataTableResModel GetUsers(DataTableParamModel model)
        {
            var dataTableResModel = new DataTableResModel();
            try
            {
                var query1 = Query.Or(
                    Query.Matches("FirstName", BsonRegularExpression.Create(new Regex(model.sSearch, RegexOptions.IgnoreCase))),
                    Query.Matches("LastName", BsonRegularExpression.Create(new Regex(model.sSearch, RegexOptions.IgnoreCase))),
                    Query.Matches("Email", BsonRegularExpression.Create(new Regex(model.sSearch, RegexOptions.IgnoreCase)))
                );
                var query2 = Query.Not(
                    Query.Matches("Email", BsonRegularExpression.Create(new Regex(model.LoginViewModel.LoginId, RegexOptions.IgnoreCase)))
                );
                var query = Query.And(query1, query2);

                var userEntityList = _userRepository.GetCollection().Find(query).Skip(model.iDisplayStart).Take(model.iDisplayLength);
                IEnumerable<UserViewModel> userViewModelList = userEntityList.Select(EntityToViewModel.ConvertUserEntityToViewModel).ToList();
                foreach (var userViewModel in userViewModelList)
                {
                    var query3 = Query.And(
                        Query.EQ("UserId", model.LoginViewModel.LoginId),
                        Query.EQ("FriendId", userViewModel.LoginId)
                        );
                    var query4 = Query.And(
                        Query.EQ("UserId", userViewModel.LoginId),
                        Query.EQ("FriendId", model.LoginViewModel.LoginId)
                        );
                    var query5 = Query.Or(query3, query4);
                    var friend = _friendRepository.GetCollection().FindOne(query5);
                    if (friend != null)
                    {
                        userViewModel.Status = friend.Status;
                        userViewModel.FriendId = friend.FriendId;
                        userViewModel.UserId = friend.UserId;
                    }
                    
                }
                long iTotalRecords = _userRepository.GetCollection().FindAll().Count();
                long iTotalDisplayRecords = _userRepository.GetCollection().Find(query).Count();
                userViewModelList = SortUserViewModelList(userViewModelList, model);
                dataTableResModel = DataTablesGridUtil.GetResDataTable(model, iTotalRecords, iTotalDisplayRecords, userViewModelList);
            }
            catch (Exception ex)
            {
                Logger.Error("An Exception occured while get User Information  : ", ex);
            }
            return dataTableResModel;
        }

        public DataTableResModel GetPendingUsers(DataTableParamModel model)
        {
            var dataTableResModel = new DataTableResModel();
            try
            {
                var query1 = Query.Or(
                    Query.Matches("FirstName", BsonRegularExpression.Create(new Regex(model.sSearch, RegexOptions.IgnoreCase))),
                    Query.Matches("LastName", BsonRegularExpression.Create(new Regex(model.sSearch, RegexOptions.IgnoreCase))),
                    Query.Matches("Email", BsonRegularExpression.Create(new Regex(model.sSearch, RegexOptions.IgnoreCase)))
                );
                var query2 = Query.Not(
                    Query.Matches("Email", BsonRegularExpression.Create(new Regex(model.LoginViewModel.LoginId, RegexOptions.IgnoreCase)))
                );
                var query = Query.And(query1, query2);

                var userEntityList = _userRepository.GetCollection().Find(query).Skip(model.iDisplayStart).Take(model.iDisplayLength);
                IEnumerable<UserViewModel> userViewModelList = userEntityList.Select(EntityToViewModel.ConvertUserEntityToViewModel).ToList();
                foreach (var userViewModel in userViewModelList)
                {
                    //var query3 = Query.And(
                    //    Query.EQ("UserId", model.LoginViewModel.LoginId),
                    //    Query.EQ("FriendId", userViewModel.LoginId),
                    //    Query.EQ("Status", "Pending")
                    //    );
                    var query4 = Query.And(
                        Query.EQ("UserId", userViewModel.LoginId),
                        Query.EQ("FriendId", model.LoginViewModel.LoginId),
                        Query.EQ("Status", "Pending")
                        );
                    //var query5 = Query.Or(query3, query4);
                    var friend = _friendRepository.GetCollection().FindOne(query4);
                    if (friend != null)
                    {
                        userViewModel.Status = friend.Status;
                        userViewModel.FriendId = friend.FriendId;
                        userViewModel.UserId = friend.UserId;
                    }
                    
                }
                IEnumerable<UserViewModel> userViewModelList2 = userViewModelList.Where(p => p.Status == "Pending");
                long iTotalRecords = _userRepository.GetCollection().FindAll().Count();
                long iTotalDisplayRecords = _userRepository.GetCollection().Find(query).Count();
                userViewModelList2 = SortUserViewModelList(userViewModelList2, model);
                dataTableResModel = DataTablesGridUtil.GetResDataTable(model, iTotalRecords, iTotalDisplayRecords, userViewModelList2);
            }
            catch (Exception ex)
            {
                Logger.Error("An Exception occured while get User Information  : ", ex);
            }
            return dataTableResModel;
        }
        public DataTableResModel GetMyFriends(DataTableParamModel model)
        {
            var dataTableResModel = new DataTableResModel();
            try
            {
                var query1 = Query.Or(
                    Query.Matches("FirstName", BsonRegularExpression.Create(new Regex(model.sSearch, RegexOptions.IgnoreCase))),
                    Query.Matches("LastName", BsonRegularExpression.Create(new Regex(model.sSearch, RegexOptions.IgnoreCase))),
                    Query.Matches("Email", BsonRegularExpression.Create(new Regex(model.sSearch, RegexOptions.IgnoreCase)))
                );
                var query2 = Query.Not(
                    Query.Matches("Email", BsonRegularExpression.Create(new Regex(model.LoginViewModel.LoginId, RegexOptions.IgnoreCase)))
                );
                var query = Query.And(query1, query2);

                var userEntityList = _userRepository.GetCollection().Find(query).Skip(model.iDisplayStart).Take(model.iDisplayLength);
                IEnumerable<UserViewModel> userViewModelList = userEntityList.Select(EntityToViewModel.ConvertUserEntityToViewModel).ToList();
                foreach (var userViewModel in userViewModelList)
                {
                    //var query3 = Query.And(
                    //    Query.EQ("UserId", model.LoginViewModel.LoginId),
                    //    Query.EQ("FriendId", userViewModel.LoginId),
                    //    Query.EQ("Status", "Pending")
                    //    );
                    var query4 = Query.And(
                        Query.EQ("UserId", userViewModel.LoginId),
                        Query.EQ("FriendId", model.LoginViewModel.LoginId),
                        Query.EQ("Status", "Accept")
                        );
                    //var query5 = Query.Or(query3, query4);
                    var friend = _friendRepository.GetCollection().FindOne(query4);
                    if (friend != null)
                    {
                        userViewModel.Status = friend.Status;
                        userViewModel.FriendId = friend.FriendId;
                        userViewModel.UserId = friend.UserId;
                    }

                }
                IEnumerable<UserViewModel> userViewModelList2 = userViewModelList.Where(p => p.Status == "Accept");
                long iTotalRecords = _userRepository.GetCollection().FindAll().Count();
                long iTotalDisplayRecords = _userRepository.GetCollection().Find(query).Count();
                userViewModelList2 = SortUserViewModelList(userViewModelList2, model);
                dataTableResModel = DataTablesGridUtil.GetResDataTable(model, iTotalRecords, iTotalDisplayRecords, userViewModelList2);
            }
            catch (Exception ex)
            {
                Logger.Error("An Exception occured while get User Information  : ", ex);
            }
            return dataTableResModel;
        }

        public ResViewModel AddFriend(ResViewModel resViewModel, UserViewModel model)
        {
            if (model.Status.Equals("Pending"))
            {
                var query = Query.And(
                        Query.EQ("Id", model.Id)
                      );
                
                var userEntity = _userRepository.GetCollection().FindOne(query);
                var friend = new Friend();
                friend.UserId = model.LoginId;
                friend.Status = model.Status;
                friend.FriendId = userEntity.Email;
                _friendRepository.GetCollection().Save(friend);
            }
            else if (model.Status.Equals("Delete"))
            {
                CancelFriendRequest(model);
            }
            else if (model.Status.Equals("Accept"))
            {
                var query = Query.And(
                        Query.EQ("Id", model.Id)
                      );
                
                var userEntity = _userRepository.GetCollection().FindOne(query);
                var query3 = Query.And(
                    Query.EQ("UserId", userEntity.Email),
                    Query.EQ("FriendId", model.LoginId)
                );
                var friend = _friendRepository.GetCollection().FindOne(query3);
                friend.Status = model.Status;
                var myFriend = new Friend();
                myFriend.UserId = friend.FriendId;
                myFriend.FriendId = friend.UserId;
                myFriend.Status = friend.Status;
                _friendRepository.GetCollection().Save(friend);
                _friendRepository.GetCollection().Save(myFriend);
            }
            resViewModel.IsSuccess = true;
            resViewModel.Code = Code.Sc1000;
            return resViewModel;
        }

        private void CancelFriendRequest(UserViewModel model)
        {
            var query = Query.And(
                        Query.EQ("Id", model.Id)
                      );

            var userEntity = _userRepository.GetCollection().FindOne(query);
            var query3 = Query.And(
                Query.EQ("UserId", model.LoginId),
                Query.EQ("FriendId", userEntity.Email)
                );
            var query4 = Query.And(
                Query.EQ("UserId", userEntity.Email),
                Query.EQ("FriendId", model.LoginId)
                );
            var query5 = Query.Or(query3, query4);


            _friendRepository.GetCollection().Remove(query5);
        }


        private IEnumerable<UserViewModel> SortUserViewModelList(IEnumerable<UserViewModel> userViewModelList,
                                                                 DataTableParamModel model)
        {

            Func<UserViewModel, string> orderingExpression = (c => model.iSortCol_0 == 0 ? c.FirstName : c.LastName);
            if (model.sSortDir_0 == Constant.Ascending)
                userViewModelList = userViewModelList.OrderBy(orderingExpression);
            else
                userViewModelList = userViewModelList.OrderByDescending(orderingExpression);
            return userViewModelList;
        }

    }
}
