﻿
using System;
using CWC.Biz.Contracts.App;
using CWC.Biz.Converter;
using CWC.Biz.Models;
using CWC.Biz.Models.App;
using CWC.Biz.Utils;
using CWC.Data.Entity;
using CWC.Lib.Repositories.Contarcts;
using Microsoft.AspNet.SignalR;

namespace CWC.Biz.Implementations.App
{
    public class QuestionManager : ManagerBase, IQuestionManager
    {

        private readonly IRepository<Question> _questionRepository;
        //private IHubContext hub;

        public QuestionManager(IRepository<Question> questionRepository) : base()
        {
            _questionRepository = questionRepository;
            //hub = GlobalHost.ConnectionManager.GetHubContext<ClientPushHub>();
        }

        public ResViewModel SaveQuestion(ResViewModel resViewModel, QuestionViewModel model)
        {
            var question = ViewToEntityModel.ConvertQuestionViewToEntity(model);
            //_questionRepository.GetCollection().Save(question);
            //hub.Clients.All.serverTime(DateTime.UtcNow.ToString());
            resViewModel.IsSuccess = true;
            resViewModel.Code = Code.Sc1000;
            return resViewModel;
        }
    }

}
