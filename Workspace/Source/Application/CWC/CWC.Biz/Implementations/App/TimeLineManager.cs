﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using CWC.Biz.Contracts.App;
using CWC.Biz.Converter;
using CWC.Biz.Models;
using CWC.Biz.Models.App;
using CWC.Biz.Utils;
using CWC.Data.Entity;
using CWC.Lib.Repositories.Contarcts;
using Microsoft.AspNet.SignalR;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

namespace CWC.Biz.Implementations.App
{
    public class TimeLineManager : ManagerBase, ITimeLineManager
    {

        private readonly IRepository<User> _userRepository;
        private readonly IRepository<TimeLine> _timeLineRepository;
        private readonly IRepository<Friend> _friendRepository;

        public TimeLineManager(IRepository<User> userRepository, IRepository<TimeLine> timeLineRepository,
            IRepository<Friend> friendRepository)
            : base()
        {
            _userRepository = userRepository;
            _timeLineRepository = timeLineRepository;
            _friendRepository = friendRepository;
        }

        public ResViewModel SaveTimeLine(ResViewModel resViewModel, TimeLineViewModel model)
        {
            model.Id = IdGenerator.GenerateOid();
            var timeLine = ViewToEntityModel.ConvertTimeLineViewToEntity(model);
            var query = Query.And(
                    Query.EQ("Email", model.LoginViewModel.LoginId)
            );

            _timeLineRepository.GetCollection().Save(timeLine);
            resViewModel.IsSuccess = true;
            resViewModel.Code = Code.Sc1000;
            return resViewModel;
        }

        public DataTableResModel GetTimeLines(DataTableParamModel model)
        {
            var dataTableResModel = new DataTableResModel();
            try
            {
                var userIdArray = new List<string>();
                userIdArray.Add(model.LoginViewModel.LoginId);

                var query1 = Query.EQ("UserId", model.LoginViewModel.LoginId);
                var query2 = Query.EQ("FriendId", model.LoginViewModel.LoginId);
                var query3 = Query.Or(query1, query2);
                var friendEntityList = _friendRepository.GetCollection().Find(query3);
                foreach (var friend in friendEntityList)
                {
                    if (model.LoginViewModel.LoginId.Equals(friend.FriendId))
                    {
                        userIdArray.Add(friend.UserId);
                    }
                    else
                    {
                        userIdArray.Add(friend.FriendId);
                    }
                }

                var query = Query.And(
                    Query.In("UserId", new BsonArray(userIdArray))
                );
                var timeLineEntityList = _timeLineRepository.GetCollection().Find(query);
                IEnumerable<TimeLineViewModel> timeLineViewModelList = timeLineEntityList.Select(EntityToViewModel.ConvertTimeLineEntityToViewModel).ToList();
                foreach (var timeLineViewModel in timeLineViewModelList)
                {
                    for (int i=0; i< timeLineViewModel.LikeUsers.Count;i++)
                    {
                        var likeUser = timeLineViewModel.LikeUsers[i];
                        var userQuery = Query.EQ("Email", likeUser);
                        var userEntity = _userRepository.GetCollection().FindOne(userQuery);
                        timeLineViewModel.LikeUsers[i] = userEntity.FirstName;
                    }
                    
                }
                
                long iTotalRecords = _timeLineRepository.GetCollection().FindAll().Count();
                long iTotalDisplayRecords = _timeLineRepository.GetCollection().FindAll().Count();
                dataTableResModel = DataTablesGridUtil.GetResDataTable(model, iTotalRecords, iTotalDisplayRecords, timeLineViewModelList);
            }
            catch (Exception ex)
            {
                Logger.Error("An Exception occured while get Time Line Information  : ", ex);
            }
            return dataTableResModel;
        }

        public ResViewModel LikeCount(ResViewModel resViewModel, TimeLineViewModel model)
        {
            var query = Query.And(
                    Query.EQ("Id", model.Id)
            );

            var timeLine = _timeLineRepository.GetCollection().FindOne(query);
            timeLine.Like = timeLine.Like + 1;
            timeLine.LikeUsers.Add(model.LoginViewModel.LoginId);
            _timeLineRepository.GetCollection().Save(timeLine);
            resViewModel.IsSuccess = true;
            resViewModel.Code = Code.Sc1000;
            return resViewModel;
        }

        public ResViewModel DeletePost(ResViewModel resViewModel, TimeLineViewModel model)
        {
            var query = Query.And(
                    Query.EQ("Id", model.Id)
            );

            _timeLineRepository.GetCollection().Remove(query);
            resViewModel.IsSuccess = true;
            resViewModel.Code = Code.Sc1000;
            return resViewModel;
        }
    }

}
