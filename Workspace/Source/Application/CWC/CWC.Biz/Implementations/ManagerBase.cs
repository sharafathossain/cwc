﻿
using Castle.Core.Logging;
using System;

namespace CWC.Biz.Implementations
{
    public abstract class ManagerBase
    {
        protected ILogger Log = NullLogger.Instance;

        protected ManagerBase()
        {
        }

        public ILogger Logger
        {
            get { return Log; }
            set { Log = value; }
        }

        protected void Commit()
        {
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        
    }
}
