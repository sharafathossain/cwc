﻿
using CWC.Biz.Models;
using CWC.Biz.Models.Security;
using CWC.Data.Entity;

namespace CWC.Biz.Contracts.Security
{
    public interface ISignInManager : IManager<Login>
    {
        ResViewModel SignIn(SignInViewModel req);

        ResViewModel SignOut(SignInViewModel req);

    }
}
