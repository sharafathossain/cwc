﻿
using CWC.Biz.Models;
using CWC.Biz.Models.App;
using CWC.Data.Entity;

namespace CWC.Biz.Contracts.App
{
    public interface IQuestionManager : IManager<Question>
    {

        ResViewModel SaveQuestion(ResViewModel resViewModel, QuestionViewModel model);

    }
}
