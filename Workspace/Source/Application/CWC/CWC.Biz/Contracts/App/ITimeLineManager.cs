﻿
using CWC.Biz.Models;
using CWC.Biz.Models.App;
using CWC.Biz.Utils;
using CWC.Data.Entity;

namespace CWC.Biz.Contracts.App
{
    public interface ITimeLineManager : IManager<Question>
    {

        ResViewModel SaveTimeLine(ResViewModel resViewModel, TimeLineViewModel model);

        DataTableResModel GetTimeLines(DataTableParamModel paramModel);

        ResViewModel LikeCount(ResViewModel resViewModel, TimeLineViewModel model);

        ResViewModel DeletePost(ResViewModel resViewModel, TimeLineViewModel model);
    }
}
