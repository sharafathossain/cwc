﻿
using CWC.Biz.Models;
using CWC.Biz.Models.App;
using CWC.Biz.Utils;
using CWC.Data.Entity;

namespace CWC.Biz.Contracts.App
{
    public interface IUserManager : IManager<User>
    {

        ResViewModel CreateUserAccount(ResViewModel resViewModel, UserViewModel model);

        ResViewModel ChangePassword(ResViewModel resViewModel, UserViewModel model);

        DataTableResModel GetUsers(DataTableParamModel model);

        DataTableResModel GetPendingUsers(DataTableParamModel model);

        DataTableResModel GetMyFriends(DataTableParamModel model);

        ResViewModel AddFriend(ResViewModel resViewModel, UserViewModel model);

    }
}
