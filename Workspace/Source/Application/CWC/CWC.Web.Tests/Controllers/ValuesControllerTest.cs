﻿
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CWC.Web.Controllers;

namespace CWC.Web.Tests.Controllers
{
    [TestClass]
    public class ValuesControllerTest
    {
        [TestMethod]
        public void Get()
        {
            var controller = new ValuesController();
            IEnumerable<string> result = controller.Get();

            // Assert
            Assert.IsNotNull(result);
            var enumerable = result as List<string> ?? result.ToList();
            Assert.AreEqual(2, enumerable.Count());
            Assert.AreEqual("value1", enumerable.ElementAt(0));
            Assert.AreEqual("value2", enumerable.ElementAt(1));
        }

        [TestMethod]
        public void GetById()
        {
            var controller = new ValuesController();
            string result = controller.Get(5);

            // Assert
            Assert.AreEqual("value", result);
        }

        [TestMethod]
        public void Post()
        {
            var controller = new ValuesController();
            controller.Post("value");
            
            // Assert

        }

        [TestMethod]
        public void Put()
        {
            var controller = new ValuesController();
            controller.Put(5, "value");

            // Assert

        }

        [TestMethod]
        public void Delete()
        {
            var controller = new ValuesController();
            controller.Delete(5);

            // Assert

        }
    }
}
