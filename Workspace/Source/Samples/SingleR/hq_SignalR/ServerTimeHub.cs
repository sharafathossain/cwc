﻿using Microsoft.AspNet.SignalR;
using System;

namespace Henriquatre.Integration.SignalR
{
    public class ServerTimeHub : Hub
    {
        public string GetServerTime()
        {
            //return this.Clients.All.serverTime(DateTime.UtcNow.ToString());
            return DateTime.UtcNow.ToString();
        }
    }
}